# DABDiscovery

Front End App - Public facing directory of DAB radio data



  npm run dev
    Starts the development server.

  npm run build
    Builds the app for production.

  npm start
    Runs the built app in production mode.

We suggest that you begin by typing:

  cd dabdiscovery
  npm run dev



PatternFly template: https://github.com/vercel/next.js/tree/canary/examples/with-patternfly
https://github.com/willieseabrook/nextjs-with-patternfly/blob/master/next.config.js


## Build and test docker container locally

### gitlab-ci.yml with kaniko

https://docs.gitlab.com/ee/ci/docker/using_kaniko.html


### dind build

  cd dabdiscovery
  docker build . -t docker.semweb.co/semweb/dabdiscovery:SNAPSHOT-latest
  docker run -p 3000:3000 docker.semweb.co/semweb/dabdiscovery:SNAPSHOT-latest

Push to docker repo with

  docker push docker.semweb.co/semweb/dabdiscovery:SNAPSHOT-latest


For building with a version tag
  docker build . -t docker.semweb.co/semweb/dabdiscovery:v0.0.1

### The experimental new way (Kankio)

Experimental Kankio build:
    gcr.io/kaniko-project/executor:debug \

  docker run \
    -v $(pwd):/app \
    -v $HOME/.docker/config.json:/kaniko/.docker/config.json:ro \
    gcr.io/kaniko-project/executor:latest \
    --dockerfile=Dockerfile \
    --context=/app \
    --destination=semweb/dabdiscovery:SNAPSHOT-latest



Note::
https://stackoverflow.com/questions/60452054/nextjs-deploy-to-a-specific-url-path



https://www.youtube.com/watch?v=SKOeZc_m3zs
https://www.youtube.com/watch?v=cf84D8v2-cM
https://keycloakthemes.com/blog/how-to-setup-sign-in-with-google-using-keycloak



https://tech.ebu.ch/docs/techreports/tr025.pdf

