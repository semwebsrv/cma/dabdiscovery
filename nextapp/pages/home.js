import React, { useEffect, useState } from "react";
import axios from 'axios'
import {
  Button,
  Card,
  CardHeader,
  CardActions,
  CardTitle,
  CardBody,
  DescriptionList,
  DescriptionListTerm,
  DescriptionListGroup,
  DescriptionListDescription,
  Gallery,
  GalleryItem,
  PageSection,
  PageSectionVariants,
  TextContent,
  Text,
} from '@patternfly/react-core';
import Link from 'next/link'

import getConfig from "next/config";
const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();

export default function Profile(props) {

  const [qr, setQr] = useState({});

  const reportOwner = info => {
    return
      info.owner != null ?
        ( <span>Owned by <Link href={"/authorities"+info.ownershortcode}>{info.owner}</Link></span> ) :
        ( <span>No owner</span> );
  }

  useEffect(() => {
    const fetchData = async () => {
      const result = await axios(publicRuntimeConfig.RR_API+'/user/getHomePageData', { params: {} } );
      setQr(result.data);
    };

    fetchData();
  }, []);

  return (
    <div>

      <PageSection isFilled key="user_home_detail">
        <DescriptionList>

          <DescriptionListGroup key="user_affiliations">
             <DescriptionListTerm>User Permissions</DescriptionListTerm>
             <DescriptionListDescription>
               <Gallery hasGutter>
                 {qr.userGrants && qr.userGrants.map((user_grant, idx) => (
                   <Card isHoverable isCompact key={user_grant.grantResourceOwner}>
                     <CardTitle>{user_grant.grantedPerm} for <Link href={"/authorities2"+
                               encodeURIComponent(user_grant.grantResourceOwner)}>{user_grant.grantResourceOwner}</Link></CardTitle>
                   </Card>
                 ))}
               </Gallery>
             </DescriptionListDescription>
          </DescriptionListGroup>

          <DescriptionListGroup key="user_services">
             <DescriptionListTerm>User Broadcast Services</DescriptionListTerm>
             <DescriptionListDescription>
               <Gallery hasGutter>
                 {qr.userServices && qr.userServices.map((user_service, idx) => (
                   <GalleryItem>
                     <Card isHoverable isCompact key={user_service.name}>
                       <CardTitle><Link href={"/services/"+user_service.slug}>{user_service.name}</Link></CardTitle>
                       <CardBody>{reportOwner(user_service)}</CardBody>
                     </Card>
                   </GalleryItem>
                 ))}
               </Gallery>
             </DescriptionListDescription>
          </DescriptionListGroup>

          <DescriptionListGroup key="user_multipexes">
             <DescriptionListTerm>User Multiplexes</DescriptionListTerm>
             <DescriptionListDescription>
               <Gallery hasGutter>
                 {qr.userMultiplexes && qr.userMultiplexes.map((user_multiplex, idx) => (
                   <GalleryItem>
                     <Card isHoverable isCompact key={user_multiplex.name}>
                       <CardTitle><Link href={"/multiplexes/"+user_multiplex.ead}>{user_multiplex.name}</Link></CardTitle>
                       <CardBody>{reportOwner(user_multiplex)}</CardBody>
                     </Card>
                   </GalleryItem>
                 ))}
               </Gallery>
             </DescriptionListDescription>
          </DescriptionListGroup>

          <DescriptionListGroup key="user_transmitters">
             <DescriptionListTerm>User Transmitters</DescriptionListTerm>
             <DescriptionListDescription>
               <Gallery hasGutter>
                 {qr.userTransmitters && qr.userTransmitters.map((user_transmitter, idx) => (
                   <GalleryItem>
                     <Card isHoverable isCompact key={user_transmitter.name}>
                       <CardTitle><Link href={"/transmitters/"+user_transmitter.slug}>{user_transmitter.name}</Link></CardTitle>
                       <CardBody>{reportOwner(user_transmitter)}</CardBody>
                     </Card>
                   </GalleryItem>
                 ))}
               </Gallery>
             </DescriptionListDescription>
          </DescriptionListGroup>





        </DescriptionList>
      </PageSection>

    </div>
  )
}
