import React, { useEffect, useState } from "react";
import { useKeycloak } from '@react-keycloak/ssr';
import AdminMenu from '../../components/AdminMenu'
import { TableComposable, Thead, Tbody, Tr, Th, Td } from '@patternfly/react-table';
import {
  Button,
  Form,
  FormGroup,
  InputGroup,
  Pagination,
  TextInput,
} from '@patternfly/react-core';
import axios from 'axios'
import Link from 'next/link'
import getConfig from "next/config";
const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();

export default function PendingOwnerRequests(props) {

  const [qr, setQr] = useState({});
  const [pageNumber, setPageNumber] = useState(1);
  const [queryString, setQueryString] = useState('');
  const [textInput, setTextInput] = useState('');
  const [refreshNeeded, setRefreshNeeded] = useState(false);
  const { keycloak, initialized } = useKeycloak()

  useEffect(() => {

    keycloak.updateToken(60);

    const fetchData = async () => {
      const query = queryString != null ? 'status:PENDING AND %'+queryString+'%' : 'status:PENDING'
      const offset = ( pageNumber - 1 ) * 10
      const result = await axios(publicRuntimeConfig.RR_API+'/admin/accessRequests', { params: {q:query, max:10, offset:offset, sort:'userName' } } );
      setQr(result.data);
    };

    fetchData();
  }, [queryString, pageNumber, refreshNeeded]);

  const onSetPage = (event, n) => {
    console.log("set page %o",n);
    setPageNumber(n)
  }

  const onPerPageSelect = (n) => {
  }

  const handleQuery = (event) => {
    setQueryString(textInput);
    event.preventDefault();
  }

  const handleTextChange = (n) => {
    setTextInput(n);
  }

  const accept = (e, row) => {
    console.log("accept %o",row);
    keycloak.updateToken(60);
    axios.post(publicRuntimeConfig.RR_API+'/admin/actions/processAccessRequest', {
               action:'ACCEPT',
               details: row
             }).
    then ( (result) => {
      console.log("got result: %o",result);
      setRefreshNeeded(true);
    })
  }

  const reject = (e, row) => {
    console.log("reject %o",row);
    keycloak.updateToken(60);
    axios.post(publicRuntimeConfig.RR_API+'/admin/actions/processAccessRequest', {
               action:'REJECT',
               details: row
             }).
    then ( (result) => {
      console.log("got result: %o",result);
      setRefreshNeeded(true);
    })
  }

  return (
    <div>
      <AdminMenu/>
      <br/>
      <Form onSubmit={handleQuery} isHorizontal>
        <FormGroup label="Username:" fieldId="username">
          <InputGroup>
            <TextInput id="qry"
                       isRequired type="text"
                       name="simple-form-name-01"
                       aria-describedby="simple-form-name-01-helper"
                       onChange={handleTextChange}
                       value={textInput} />
            <Button type="submit" variant="primary">Search</Button>
          </InputGroup>
        </FormGroup>

      </Form>

      <Pagination
        itemCount={qr?.totalCount}
        perPage={10}
        page={pageNumber}
        onSetPage={onSetPage}
        widgetId="pagination-options-menu-top"
        onPerPageSelect={onPerPageSelect}
      />

      <TableComposable aria-label="A list of DAB multiplexes" variant='compact'>
        <Thead>
          <Tr>
            <Th>Requesting User</Th>
            <Th>Requested Permission</Th>
            <Th>Against</Th>
            <Th>Via</Th>
            <Th>Notes</Th>
            <Th>Requested on</Th>
            <Th>Action</Th>
          </Tr>
        </Thead>
        <Tbody>
          {qr.resultList && qr.resultList.map((row, rowIndex) => (
            <Tr key={'rr_mp_'+row.id}>
              <Td><Link href={"/admin/pendingAccessRequests/"+row.id}>{row.userName}</Link></Td>
              <Td>{row.requestedPerm}</Td>
              <Td>{row.resourceType}:<strong>{row.resourceLabel}</strong></Td>
              <Td>
                { ( row.authorityId == null && 'A new authority :'+row.authorityName ) }
                { ( row.authorityId != null && 'The existing authority' ) }
              </Td>
              <Td>{row.notes}</Td>
              <Td>{row.dateOfRequest}</Td>
              <Td>
                <Button type="submit" variant="primary" onClick={(e) => { reject(e,row) }}>Reject</Button>
                &nbsp;
                <Button type="submit" variant="primary" onClick={(e) => { accept(e,row) }}>Accept</Button>
              </Td>
            </Tr>
          ))}
        </Tbody>
      </TableComposable>

    </div>
  )
}
