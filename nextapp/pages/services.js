import React, { useEffect, useState } from "react";
import axios from 'axios'
import { TableComposable, Thead, Tbody, Tr, Th, Td } from '@patternfly/react-table';
import {
  Button,
  Form,
  FormGroup,
  InputGroup,
  Pagination,
  TextInput,
} from '@patternfly/react-core';
import { useKeycloak } from '@react-keycloak/ssr';
import Link from 'next/link'
import { useRouter } from 'next/router'
import getConfig from "next/config";
const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();



export default function Services(props) {

  const [qr, setQr] = useState(props.initialMultiplexes);
  const [pageNumber, setPageNumber] = useState(1);
  const [queryString, setQueryString] = useState('');
  const [textInput, setTextInput] = useState('');

  const router = useRouter()
  const { keycloak, initialized } = useKeycloak()
  const user_data = initialized && keycloak.authenticated ? keycloak.tokenParsed : null;
  const is_admin = user_data?.realm_access?.roles?.includes('ADMIN')
  const is_accredited = user_data?.realm_access?.roles?.includes('ACCREDITED')

  useEffect(() => {
    console.log("Services::useEffect queryString=%s, pageNumber=%d",queryString,pageNumber);
    const fetchData = async () => {
      const query = queryString != null ? '%'+queryString+'%' : '%'
      const offset = ( pageNumber - 1 ) * 10
      const result = await axios(publicRuntimeConfig.RR_API+'/public/services', { params: {q:query, max:10, offset:offset, sort:'name' } } );
      setQr(result.data);
    };
 
    fetchData();
  }, [queryString, pageNumber]);

  const onSetPage = (event, n) => {
    console.log("set page %o",n);
    setPageNumber(n)
  }

  const onPerPageSelect = (n) => {
  }

  const handleQuery = (event) => {
    setQueryString(textInput);
    setPageNumber(1);
    event.preventDefault();
  }

  const handleTextChange = (n) => {
    setTextInput(n);
  }

  const goToCreateService = () => {
    router.push("/services/admin/create");
  }

  console.log("Services...");

  return (
    <>
      <Form onSubmit={handleQuery} isHorizontal>
        <FormGroup label="Service name:" fieldId="service-name">
          <InputGroup>
            <TextInput id="qry"
                       isRequired type="text"
                       name="simple-form-name-01"
                       aria-describedby="simple-form-name-01-helper"
                       onChange={handleTextChange}
                       value={textInput} />
            <Button type="submit" variant="primary">Search</Button>
            {
              ( ( is_admin || is_accredited ) && ( 
                <span>&nbsp; <Button type="submit" onClick={goToCreateService} variant="primary">New</Button></span>
              ) ) 
            }
          </InputGroup>
        </FormGroup>
        
      </Form>
 
      <Pagination
        itemCount={qr?.totalCount}
        perPage={10}
        page={pageNumber}
        onSetPage={onSetPage}
        widgetId="pagination-options-menu-top"
        onPerPageSelect={onPerPageSelect}
      />

      <TableComposable aria-label="A list of DAB services">
        <Thead>
          <Tr>
            <Th>Service name</Th>
            <Th>Service Id</Th>
            <Th>Slug</Th>
          </Tr>
        </Thead>
        <Tbody>
          {qr.resultList && qr.resultList.map((row, rowIndex) => (
            <Tr key={'rr_service'+row.slug}>
              <Td><Link href={"/services/"+row.slug}>{row.name}</Link></Td>
              <Td>{row.sid}</Td>
              <Td>{row.slug}</Td>
            </Tr>
          ))}
        </Tbody>
      </TableComposable>

    </>
  )
}

export async function getServerSideProps(context) {
  const res = await fetch(publicRuntimeConfig.RR_API+'/public/services?q=%25&max=10&offset=0')
  const data = await res.json()
  return { props: { initialMultiplexes: data } }
}

