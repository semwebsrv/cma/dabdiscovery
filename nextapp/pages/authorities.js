import React, { useEffect, useState } from "react";
import axios from 'axios'
import { TableComposable, Thead, Tbody, Tr, Th, Td } from '@patternfly/react-table';
import {
  Button,
  Form,
  FormGroup,
  InputGroup,
  Pagination,
  TextInput,
} from '@patternfly/react-core';
import Link from 'next/link'
import getConfig from "next/config";
const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();


export default function Services(props) {

  const [qr, setQr] = useState(props.initialMultiplexes);
  const [pageNumber, setPageNumber] = useState(1);
  const [queryString, setQueryString] = useState('');
  const [textInput, setTextInput] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      const query = queryString != null ? '%'+queryString+'%' : '%'
      const offset = ( pageNumber - 1 ) * 10
      const result = await axios(publicRuntimeConfig.RR_API+'/public/parties', { params: {q:query, max:10, offset:offset, sort:'name' } } );
      setQr(result.data);
    };
 
    fetchData();
  }, [queryString, pageNumber]);

  const onSetPage = (event, n) => {
    console.log("set page %o",n);
    setPageNumber(n)
  }

  const onPerPageSelect = (n) => {
  }

  const handleQuery = (event) => {
    setQueryString(textInput);
    event.preventDefault();
  }

  const handleTextChange = (n) => {
    setTextInput(n);
  }

  return (
    <div>
      <Form onSubmit={handleQuery} isHorizontal>
        <FormGroup label="Authority name:" fieldId="authority-name">
          <InputGroup>
            <TextInput id="qry"
                       isRequired type="text"
                       name="simple-form-name-01"
                       aria-describedby="simple-form-name-01-helper"
                       onChange={handleTextChange}
                       value={textInput} />
            <Button type="submit" variant="primary">Search</Button>
          </InputGroup>
        </FormGroup>
        
      </Form>
 
      <Pagination
        itemCount={qr?.totalCount}
        perPage={10}
        page={pageNumber}
        onSetPage={onSetPage}
        widgetId="pagination-options-menu-top"
        onPerPageSelect={onPerPageSelect}
      />

      <TableComposable aria-label="A list of Authorities">
        <Thead>
          <Tr>
            <Th>Authority name</Th>
            <Th>Authority shortcode</Th>
            <Th>Full shortcode</Th>
          </Tr>
        </Thead>
        <Tbody>
          {qr.resultList && qr.resultList.map((row, rowIndex) => (
            <Tr key={'rrauth'+row.shortcode}>
              <Td><Link href={"/authorities/"+row.shortcode}>{row.name}</Link></Td>
              <Td>{row.shortcode}</Td>
              <Td>{row.fqshortcode}</Td>
            </Tr>
          ))}
        </Tbody>
      </TableComposable>

    </div>
  )
}

export async function getServerSideProps(context) {
  const res = await fetch(publicRuntimeConfig.RR_API+'/public/parties?q=%25&max=10&offset=0')
  const data = await res.json()
  return { props: { initialMultiplexes: data } }
}

