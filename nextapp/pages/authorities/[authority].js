import React, { useEffect, useState } from "react";
import axios from 'axios'
import { TableComposable, Thead, Tbody, Tr, Th, Td } from '@patternfly/react-table';
import Link from 'next/link'
import { useRouter } from 'next/router'
import {
  DescriptionList,
  DescriptionListTerm,
  DescriptionListGroup,
  DescriptionListDescription,
  PageSection,
  PageSectionVariants,
  TextContent,
  Text,
} from '@patternfly/react-core';
import getConfig from "next/config";
const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();

export default function Authority() {

  const [qr, setQr] = useState({});
  const router = useRouter()

  useEffect(() => {
    const { authority } = router.query
    const fetchData = async () => {
      const result = await axios(publicRuntimeConfig.RR_API+'/public/parties/'+authority, { params: {setname:'full'} } );
      setQr(result.data);
    };
 
    fetchData();
  }, []);

  return (
    <>
      <PageSection variant={PageSectionVariants.light}>
        <TextContent>
          <Text component="h1">Authority: {qr.name}</Text>
        </TextContent>
      </PageSection>

      <PageSection isFilled>
        <DescriptionList>
           <DescriptionListGroup>
             <DescriptionListTerm>Name</DescriptionListTerm>
             <DescriptionListDescription>{qr.name}</DescriptionListDescription>
          </DescriptionListGroup>
            <DescriptionListGroup>
             <DescriptionListTerm>Data</DescriptionListTerm>
             <DescriptionListDescription>{ JSON.stringify(qr) }</DescriptionListDescription>
          </DescriptionListGroup>
        </DescriptionList>
      </PageSection>
    </>

  )
}
