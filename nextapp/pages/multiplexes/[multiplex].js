import React, { useEffect, useState } from "react";
import axios from 'axios'
import { TableComposable, Thead, Tbody, Tr, Th, Td } from '@patternfly/react-table';
import Link from 'next/link'
import { useRouter } from 'next/router'
import {
  Button,
  Card,
  CardHeader,
  CardActions,
  CardTitle,
  CardBody,
  DescriptionList,
  DescriptionListTerm,
  DescriptionListGroup,
  DescriptionListDescription,
  Gallery,
  GalleryItem,
  PageSection,
  PageSectionVariants,
  TextContent,
  Text,
  Title,
} from '@patternfly/react-core';
import { useKeycloak } from '@react-keycloak/ssr';

import OwnershipInfo from '../../components/OwnershipInfo'

import getConfig from "next/config";
const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();

export default function Multiplex() {

  const { keycloak, initialized } = useKeycloak()
  const user_data =  initialized && keycloak.authenticated ? keycloak.tokenParsed : null;
  const [qr, setQr] = useState({});
  const [userContextInfo, setUserContextInfo] = useState({});
  const router = useRouter()
  const user_is_owner = false;

  keycloak.updateToken(60);

  let description = null;
  if ( qr.description != null ) {
    description = qr.description;
  }
  else {
    if ( user_is_owner ) {
      description = 'Please use the edit button to provide a description'
    }
    else {
      if ( keycloak?.authenticated == true ) {
        description = 'We do not currently have a description for this multiplex. If you are responsible for this multiplex, please consider adopting the record below and providing one'
      }
      else {
        description = 'We do not currently have a description for this multiplex. If you are responsible for this multiplex, please log in and place a request to adopt this record to provide one '
      }
    }
  }

  useEffect(() => {
    const { multiplex } = router.query
    // const axios_config = {
    //   headers: {
    //     'Authorization':'Bearer '+keycloak?.token
    //   }
    // }
    const fetchData = async () => {
      const result = await axios(publicRuntimeConfig.RR_API+'/public/multiplexes/'+multiplex, 
                                 { params: {setname:'full'} } );
      setQr(result.data);

      if ( user_data != null ) {
        const r2 = await axios(publicRuntimeConfig.RR_API+'/multiplex/'+result.data.id+'/userContext', {});
        setUserContextInfo(r2.data);
      }

    };

    fetchData();
  }, []);

  const goToAdmin = () => {
    const { multiplex } = router.query
    router.push("/multiplexes/"+multiplex+"/admin");
  }

  const admin_link = userContextInfo?.userIsEditor ? (
    <Button className={["pf-u-float-right","pf-u-text-align-right"]} onClick={goToAdmin} variant="danger"><i className="pf-icon pf-icon-edit"></i></Button>
  ) : null;

  const send_to_ofcom = userContextInfo?.userIsEditor ? (
    <Button className={["pf-u-float-right","pf-u-text-align-right"]} onClick={sendToOfcom}>Email to OFCOM</Button> 
  ) : null;

  const sendToOfcom = () => {
  }

  const downloadFile = ({ data, fileName, fileType }) => {
    // Create a blob with the data we want to download as a file
    const blob = new Blob([data], { type: fileType })
    // Create an anchor element and dispatch a click event on it
    // to trigger a download
    const a = document.createElement('a')
    a.download = fileName
    a.href = window.URL.createObjectURL(blob)
    const clickEvt = new MouseEvent('click', {
      view: window,
      bubbles: true,
      cancelable: true,
    })
    a.dispatchEvent(clickEvt)
    a.remove()
  }

  const exportToCsv = e => {
    e.preventDefault()
    console.log("exportToJson "+'mp+'+router.query.multiplex+'.json');

    const csv_data = 'Property, Value\n'+
                     'Multiplex ID, "'+router.query.multiplex+'"';
    
    downloadFile({
      data: csv_data,
      fileName: 'mp-'+router.query.multiplex+'.csv',
      fileType: 'text/csv',
    })
  }

  const exportToJson = e => {
    e.preventDefault()
    console.log("exportToJson "+'mp+'+router.query.multiplex+'.json');
    downloadFile({
      data: JSON.stringify(qr),
      fileName: 'mp-'+router.query.multiplex+'.json',
      fileType: 'text/json',
    })
  }

  return (
    <>
      <PageSection variant={PageSectionVariants.light} key="multiplex_header">
        <img src={qr.logo128} style={{float:'right', overflow:'auto', padding:'2px'}}/>
        <TextContent>
          <Text component="h1">Multiplex: <a href={qr.homepage}>{qr.name}</a> {admin_link} {send_to_ofcom} &nbsp;
            <Button onClick={exportToJson}>Download (JSON)</Button> &nbsp;
            <Button onClick={exportToCsv}>Download (CSV)</Button> &nbsp;
          </Text>
          <Text component="p">{ description }</Text>
          <OwnershipInfo name={qr.name} owner={qr.owner} type="Multiplex" resourceId={qr.id} userData={user_data} lastUpdated={qr.lastUpdated}/>
        </TextContent>
      </PageSection>

      <PageSection isFilled key="multiplex_detail">

        <DescriptionList>


          <DescriptionListGroup>
            <DescriptionListTerm>Primary Details</DescriptionListTerm>
            <DescriptionListDescription>
              <Gallery hasGutter>
                <GalleryItem> <Title headingLevel="h3">Coverage Area</Title> {qr.area ?? 'Unknown'} </GalleryItem>
                <GalleryItem> <Title headingLevel="h3">Region</Title> {qr.region?.value ?? 'Unknown'} </GalleryItem>
              </Gallery>
            </DescriptionListDescription>
           
            <DescriptionListTerm>Social Media Info</DescriptionListTerm>
            <DescriptionListDescription>

              <Gallery hasGutter>
                <GalleryItem> <Title headingLevel="h3">EID</Title> {qr.ead ?? 'Unknown'} </GalleryItem>
                <GalleryItem> <Title headingLevel="h3">Homepage</Title> {
                  qr.homepage ? <a href={qr.homepage}>{qr.homepage}</a> : 'Unknown'
                } </GalleryItem>
                <GalleryItem> <Title headingLevel="h3">Twitter</Title> {qr.twitterHandle ?? 'Unknown'} </GalleryItem>
                <GalleryItem> <Title headingLevel="h3">Facebook</Title> {qr.fbpage ?? 'Unknown'} </GalleryItem>
                <GalleryItem> <Title headingLevel="h3">Instagram</Title> {qr.instagramAddr ?? 'Unknown'} </GalleryItem>
                <GalleryItem> <Title headingLevel="h3">Youtube</Title> {qr.youtubeChannel ?? 'Unknown'} </GalleryItem>

              </Gallery>
            </DescriptionListDescription>

            <DescriptionListTerm>Address Info</DescriptionListTerm>
            <DescriptionListDescription>


              <Gallery hasGutter>
                <GalleryItem> <Title headingLevel="h3">Address Line#1</Title>{qr.addrLine1 ?? 'Unknown'}</GalleryItem>
                <GalleryItem> <Title headingLevel="h3">Address Line#2</Title>{qr.addrLine2 ?? 'Unknown'}</GalleryItem>
                <GalleryItem> <Title headingLevel="h3">Town</Title> {qr.town ?? 'Unknown'} </GalleryItem>
                <GalleryItem> <Title headingLevel="h3">Postcode</Title> {qr.addrPostcode ?? 'Unknown'} </GalleryItem>
                <GalleryItem> <Title headingLevel="h3">Telephone</Title> {qr.tel ?? 'Unknown'} </GalleryItem>
              </Gallery>

            </DescriptionListDescription>
          </DescriptionListGroup>



          <DescriptionListGroup key="transmitter_group">
             <DescriptionListTerm>Transmitters in this ensemble</DescriptionListTerm>
             <DescriptionListDescription>
               <Gallery hasGutter>
                 {qr.transmitters && qr.transmitters.map((mtr, idx) => (
                   <Card isHoverable isCompact key={mtr.name}>
                     <CardTitle><Link href={"/transmitters/"+mtr.transmitter.slug}>{mtr.transmitter.name}</Link></CardTitle>
                     <CardBody></CardBody>
                   </Card>
                 ))}
               </Gallery>
             </DescriptionListDescription>
          </DescriptionListGroup>

          <DescriptionListGroup key="service_group">
             <DescriptionListTerm>Services in this ensemble</DescriptionListTerm>
             <DescriptionListDescription>
               <Gallery hasGutter>
                 {qr.bearers && qr.bearers.map((svc, idx) => (
                   <Card isHoverable isCompact key={svc.id}>
                     <CardTitle><Link href={"/services/"+svc.service.sid}>{svc.service.name}</Link></CardTitle>
                     <CardBody>
                       Type: {svc.type} <br/>
                       Capacity Units: {svc.capacityUnits ?? 'Unknown'}
                     </CardBody>
                   </Card>
                 ))}
               </Gallery>
             </DescriptionListDescription>
          </DescriptionListGroup>

        </DescriptionList>
      </PageSection>
    </>
  )

}
