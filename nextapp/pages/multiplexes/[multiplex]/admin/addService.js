import React, { useEffect, useState } from "react";
import axios from 'axios'
import { TableComposable, Thead, Tbody, Tr, Th, Td } from '@patternfly/react-table';
import Link from 'next/link'
import { useRouter } from 'next/router'
import {
  ActionGroup,
  Button,
  Card,
  CardHeader,
  CardActions,
  CardTitle,
  CardBody,
  DatePicker,
  DescriptionList,
  DescriptionListTerm,
  DescriptionListGroup,
  DescriptionListDescription,
  Form,
  FormGroup,
  FormSelect,
  FormSelectOption,
  Gallery,
  Nav,
  NavItem,
  NavList,
  Select,
  SelectOption,
  SelectVariant,
  TextInput,
  TextArea,
  Toolbar,
  ToolbarContent,
  ToolbarItem,
  PageSection,
  PageSectionVariants,
  TextContent,
  Text,
} from '@patternfly/react-core';

import { useKeycloak } from '@react-keycloak/ssr';

import getConfig from "next/config";
const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();


export default function MultiplexAdmin() {

  const { keycloak, initialized } = useKeycloak()
  const user_data =  initialized && keycloak.authenticated ? keycloak.tokenParsed : null;
  const [pageState, setPageState] = useState({publicMultiplexData:{}, newDABBearerData:{}});
  const [userContextInfo, setUserContextInfo] = useState({});
  const router = useRouter()
  const [ isServiceTypedownOpen, setIsServiceTypedownOpen ] = useState(false);
  const [ selectedService, setSelectedService ] = useState(null);
  const [ options, setOptions ] = useState([]);
  const [ serviceFilterValue, setServiceFiltervalue ] = useState([]);

  keycloak.updateToken(60);


  useEffect(() => {
    const { multiplex } = router.query
    const fetchData = async () => {
      const result = await axios(publicRuntimeConfig.RR_API+'/public/multiplexes/'+multiplex,
                                 { params: {setname:'full'} } );
      // Configure the multiplex half of the relationship
      // ...pageState spread the unchanged values and then override the new ones
      setPageState({
                         ...pageState, 
                         ['publicMultiplexData']: result.data,
                         ['newDABBearerData']: { multiplex: {id: result.data.id }, type:'DAB', startDate: new Date().toISOString(0,10).substring(0,10) }
                   });
    }

    fetchData();
  }, []);

  useEffect(() => {
    // fetch services
    const fetchData = async () => {
      const services_qr = await axios(publicRuntimeConfig.RR_API+'/public/services',
                                 {
                                   params: {
                                     setname:'brief',
                                     q:'%'+serviceFilterValue+'%',
                                     max:10,
                                     offset:0
                                   }
                                 });
      console.log("Got services result %o",services_qr);
      if ( services_qr && services_qr.data && services_qr.data.resultList) {
        const option_data = services_qr.data.resultList.map ( ( p, index ) => {
          return {
            id: p.id,
            value: {
              resource: p,
              toString: () => { return p.name }
            }
          }
        });
        setOptions(option_data);
      }

    };

    fetchData();
  }, [serviceFilterValue]);

  // Active path can be found at router.pathname - style for active page should be className=pf-m-current
  const handleTextInputChange = (prop, value) => {
    // this.setState({ value });
    var formData = pageState.newDABBearerData
    formData[prop]=value
    setPageState({...pageState, ['newDABBearerData']: formData});
    console.log("updated newDABBearerData : %o",formData);
  };


  const handleSubmit = (evt) => {
    console.log("Submit....%o",pageState.newDABBearerData);

    evt.preventDefault();
    const postData = async() => {
      const result = await axios.post(publicRuntimeConfig.RR_API+'/bearer',
                                     pageState.newDABBearerData,
                                     { headers: { 'Authorization':'Bearer '+keycloak.token } } )
      console.log("Result of put %o : %o",pageState.newDABBearerData.id,result);
    }

    postData();
  };

  const onToggle = () => {
    setIsServiceTypedownOpen( !isServiceTypedownOpen )
  }

  const onSelect = (event, selection, isPlaceholder) => {
    console.log("onSelect %o",selection);
    if ( ( selection != null ) &&
         ( selection.resource?.id != null ) ) {
      setSelectedService(selection);

      // Set the ID of the service in our new bearer to the ID of the selected service
      var formData = pageState.newDABBearerData
      formData.service={id:selection.resource?.id}
      setPageState({...pageState, ['newDABBearerData']: formData});

      // Hide the typedown
      setIsServiceTypedownOpen(false)
    }
  }

  const clearSelection = () => {
    setSelectedServiceParty(null);
  }

  const updateServiceFilter = (value) => {
    console.log("updateServiceFilter %s",value);
    setIsServiceTypedownOpen(true)
    setServiceFiltervalue(value)
  }

  const passthroughFilter = e => {
    options.map((option, index) => {
      return (
        <SelectOption
            key={index}
            value={option.value}
            {...(option.description && { description: option.description })}
        />
      );
    })
  }

  if ( pageState.publicMultiplexData != null ) {
    return (
      <>
        <Toolbar id="AdminToolbar">
          <ToolbarContent>
            <ToolbarItem variant="label">
              Multiplex Admin: <a href={pageState?.publicMultiplexData?.homepage}>{pageState?.publicMultiplexData?.name}</a>
            </ToolbarItem>
            <ToolbarItem>
              <Nav variant="tertiary" theme="dark">
                <NavList>
                    <Link href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin'} >
                      <a className="pf-c-nav__link" href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin'}>General</a>
                    </Link>
                    <Link href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin/occupancy'} >
                      <a className="pf-c-nav__link" href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin/occupancy'}>Occupancy</a>
                    </Link>
                    <Link href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin/transmitters'} >
                      <a className="pf-c-nav__link" href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin/transmitters'}>Transmitters</a>
                    </Link>
                    <Link href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin/addService'} >
                      <a className="pf-c-nav__link pf-m-current" href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin/addService'}>Add Service</a>
                    </Link>
                </NavList>
              </Nav>
            </ToolbarItem>
          </ToolbarContent>
        </Toolbar>
  

        <PageSection isFilled key="multiplex_general">
          <Form>
            <DescriptionList>
              <DescriptionListGroup key="service_to_add_group">
                <DescriptionListTerm>Service to add</DescriptionListTerm>
                <DescriptionListDescription>
                  <Select
                    variant={SelectVariant.typeahead}
                    typeAheadAriaLabel="Service"
                    onToggle={onToggle}
                    onSelect={onSelect}
                    onClear={clearSelection}
                    onTypeaheadInputChanged={updateServiceFilter}
                    onFilter={passthroughFilter}
                    selections={selectedService}
                    isOpen={isServiceTypedownOpen}
                    aria-labelledby="service-typedown-1"
                    isInputValuePersisted={true}
                    placeholderText="Service name...."
                    isDisabled={false}
                  >
                    {options.map((option, index) => (
                      <SelectOption key={option.id} value={option.value} index={index} />
                    ))}
                  </Select>
                </DescriptionListDescription>
              </DescriptionListGroup>
  
              <DescriptionListGroup key="dsp_license_group">
                <DescriptionListTerm>DSP License</DescriptionListTerm>
                <DescriptionListDescription>
                  <TextInput id="ti-ds-license" onChange={(value) => handleTextInputChange('dsp_license',value)}/>
                </DescriptionListDescription>
              </DescriptionListGroup>
  
              <DescriptionListGroup key="occupancy_type_group">
                <DescriptionListTerm>Type</DescriptionListTerm>
                <DescriptionListDescription>
                  DAB
                </DescriptionListDescription>
              </DescriptionListGroup>
  
              <DescriptionListGroup key="occupancy_start_group">
                <DescriptionListTerm>Start Date</DescriptionListTerm>
                <DescriptionListDescription>
                  <DatePicker value={pageState.newDABBearerData.startDate} />
                </DescriptionListDescription>
              </DescriptionListGroup>
  
              <DescriptionListGroup key="occupancy_end_group">
                <DescriptionListTerm>End Date</DescriptionListTerm>
                <DescriptionListDescription>
                  <DatePicker />
                </DescriptionListDescription>
              </DescriptionListGroup>
  
              <DescriptionListGroup key="sampling_rate_group">
                <DescriptionListTerm>Sampling Rate</DescriptionListTerm>
                <DescriptionListDescription>
                  <TextInput id="ti-sampling-rate"/>
                </DescriptionListDescription>
              </DescriptionListGroup>
  
              <DescriptionListGroup key="mode_group">
                <DescriptionListTerm>Mode</DescriptionListTerm>
                <DescriptionListDescription>
                  <TextInput id="ti-mode" value="Model 1 only"/>
                </DescriptionListDescription>
              </DescriptionListGroup>
  
              <DescriptionListGroup key="protection_group">
                <DescriptionListTerm>Protection</DescriptionListTerm>
                <DescriptionListDescription>
                  <TextInput id="ti-protection" value="Select 1A through 4A"/>
                </DescriptionListDescription>
              </DescriptionListGroup>
  
              <DescriptionListGroup key="cu_group">
                <DescriptionListTerm>Capacity Units</DescriptionListTerm>
                <DescriptionListDescription>
                  <TextInput id="ti-cu" />
                </DescriptionListDescription>
              </DescriptionListGroup>
  
              <DescriptionListGroup key="parity_group">
                <DescriptionListTerm>Parity</DescriptionListTerm>
                <DescriptionListDescription>
                  <TextInput id="ti-parity"/>
                </DescriptionListDescription>
              </DescriptionListGroup>
  
  
            </DescriptionList>
  
              <ActionGroup>
                <Button variant="primary" onClick={handleSubmit}>Submit</Button>
                <Button variant="link">Cancel</Button>
              </ActionGroup>
  
          </Form>
        </PageSection>
      </>
    );
  }
  else {
    return null;
  }
}

