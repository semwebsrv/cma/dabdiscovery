import React, { useEffect, useState } from "react";
import axios from 'axios'
import { TableComposable, Thead, Tbody, Tr, Th, Td } from '@patternfly/react-table';
import Link from 'next/link'
import { useRouter } from 'next/router'
import {
  Button,
  Card,
  CardHeader,
  CardActions,
  CardTitle,
  CardBody,
  DescriptionList,
  DescriptionListTerm,
  DescriptionListGroup,
  DescriptionListDescription,
  Form,
  FormGroup,
  FormSelect,
  FormSelectOption,
  Gallery,
  Nav,
  NavItem,
  NavList,
  TextInput,
  TextArea,
  Toolbar,
  ToolbarContent,
  ToolbarItem,
  PageSection,
  PageSectionVariants,
  TextContent,
  Text,
} from '@patternfly/react-core';

import { useKeycloak } from '@react-keycloak/ssr';

import getConfig from "next/config";
const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();

export default function MultiplexAdmin() {

  const { keycloak, initialized } = useKeycloak()
  const user_data =  initialized && keycloak.authenticated ? keycloak.tokenParsed : null;
  const [pageState, setPageState] = useState({});
  const [userContextInfo, setUserContextInfo] = useState({});
  const router = useRouter()

  keycloak.updateToken(60);


  useEffect(() => {
    const { multiplex } = router.query
    const fetchData = async () => {
      const result = await axios(publicRuntimeConfig.RR_API+'/public/multiplexes/'+multiplex,
                                 { params: {setname:'full'} } );
      setPageState({...pageState, ['publicMultiplexData']: result.data});
    };

    fetchData();
  }, []);

  // Active path can be found at router.pathname - style for active page should be className=pf-m-current

  if ( pageState.publicMultiplexData != null ) {
    return (
      <>
        <Toolbar id="AdminToolbar">
          <ToolbarContent>
            <ToolbarItem variant="label">
              Multiplex Admin: <a href={pageState?.publicMultiplexData?.homepage}>{pageState?.publicMultiplexData?.name}</a>
            </ToolbarItem>
            <ToolbarItem>
              <Nav variant="tertiary" theme="dark">
                <NavList>
                    <Link href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin'} >
                      <a className="pf-c-nav__link" href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin'}>General</a>
                    </Link>
                    <Link href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin/occupancy'} >
                      <a className="pf-c-nav__link" href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin/occupancy'}>Occupancy</a>
                    </Link>
                    <Link href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin/transmitters'} >
                      <a className="pf-c-nav__link pf-m-current" href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin/transmitters'}>Transmitters</a>
                    </Link>
                </NavList>
              </Nav>
            </ToolbarItem>
          </ToolbarContent>
        </Toolbar>
  
        <PageSection isFilled key="multiplex_general">
        </PageSection>
      </>
    );
  }
  else {
    return null;
  }
}

