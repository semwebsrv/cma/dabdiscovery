import React, { useEffect, useState } from "react";
import axios from 'axios'
import { TableComposable, Thead, Tbody, Tr, Th, Td } from '@patternfly/react-table';
import Link from 'next/link'
import { useRouter } from 'next/router'
import {
  Button,
  Card,
  CardHeader,
  CardActions,
  CardTitle,
  CardBody,
  DescriptionList,
  DescriptionListTerm,
  DescriptionListGroup,
  DescriptionListDescription,
  Form,
  FormGroup,
  FormSelect,
  FormSelectOption,
  Gallery,
  Nav,
  NavItem,
  NavList,
  TextInput,
  TextArea,
  Toolbar,
  ToolbarContent,
  ToolbarItem,
  PageSection,
  PageSectionVariants,
  TextContent,
  Text,
} from '@patternfly/react-core';

import { useKeycloak } from '@react-keycloak/ssr';

import getConfig from "next/config";
const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();

export default function MultiplexAdmin() {

  const { keycloak, initialized } = useKeycloak()
  const user_data =  initialized && keycloak.authenticated ? keycloak.tokenParsed : null;
  const [pageState, setPageState] = useState({});
  const [userContextInfo, setUserContextInfo] = useState({});
  const router = useRouter()
  const { multiplex, occupant } = router.query

  keycloak.updateToken(60);


  useEffect(() => {
    const fetchData = async () => {
      const result = await axios(publicRuntimeConfig.RR_API+'/public/multiplexes/'+multiplex,
                                 { params: {setname:'full'} } );
      setPageState({...pageState, ['occupantData']: result.data});
    };

    fetchData();
  }, []);

  return (
      <>
        Occupant {occupant}
        {JSON.stringify(pageState.occupantData)}

        <PageSection isFilled key="occupant_general">
          <Form>
            <Accordion asDefinitionList={false}>
              <AccordionItem>
                <AccordionToggle isExpanded={true}>Occupant</AccordionToggle>
                <AccordionContent>
                  <Gallery hasGutter>
                    <GalleryItem>
                      <FormGroup label="Name" isRequired fieldId="horizontal-form-capacity-units">
                        <TextInput value={pageState.occupantData.capacityUnits}
                                   isRequired
                                   type="text"
                                   onChange={(value) => handleTextInputChange('capacityUnits',value)}
                                   id="horizontal-form-capacity-units"
                                   name="Capacity Units" />
                      </FormGroup>
                    </GalleryItem>
                  </Gallery>
                </AccordionContent>
              </AccordionItem>
            </Accordion>
          </Form>
        </PageSection>
      </>
  )
}

