import React, { useEffect, useState } from "react";
import axios from 'axios'
import { TableComposable, Thead, Tbody, Tr, Th, Td } from '@patternfly/react-table';
import Link from 'next/link'
import { useRouter } from 'next/router'
import {
  Button,
  Card,
  CardHeader,
  CardActions,
  CardTitle,
  CardBody,
  DescriptionList,
  DescriptionListTerm,
  DescriptionListGroup,
  DescriptionListDescription,
  Form,
  FormGroup,
  FormSelect,
  FormSelectOption,
  Gallery,
  Nav,
  NavItem,
  NavList,
  TextInput,
  TextArea,
  Toolbar,
  ToolbarContent,
  ToolbarItem,
  PageSection,
  PageSectionVariants,
  TextContent,
  Text,
} from '@patternfly/react-core';

import { useKeycloak } from '@react-keycloak/ssr';

import getConfig from "next/config";
const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();

export default function MultiplexAdmin() {

  const { keycloak, initialized } = useKeycloak()
  const user_data =  initialized && keycloak.authenticated ? keycloak.tokenParsed : null;
  const [pageState, setPageState] = useState({});
  const [userContextInfo, setUserContextInfo] = useState({});
  const router = useRouter()

  keycloak.updateToken(60);


  useEffect(() => {
    const { multiplex } = router.query
    const fetchData = async () => {
      const result = await axios(publicRuntimeConfig.RR_API+'/public/multiplexes/'+multiplex,
                                 { params: {setname:'full'} } );
      setPageState({...pageState, ['publicMultiplexData']: result.data});
    };

    fetchData();
  }, []);

  // Active path can be found at router.pathname - style for active page should be className=pf-m-current

  if ( pageState.publicMultiplexData != null ) {
    return (
      <>
        <Toolbar id="AdminToolbar">
          <ToolbarContent>
            <ToolbarItem variant="label">
              Multiplex Admin: <a href={pageState?.publicMultiplexData?.homepage}>{pageState?.publicMultiplexData?.name}</a>
            </ToolbarItem>
            <ToolbarItem>
              <Nav variant="tertiary" theme="dark">
                <NavList>
                    <Link href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin'} >
                      <a className="pf-c-nav__link" href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin'}>General</a>
                    </Link>
                    <Link href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin/occupancy'} >
                      <a className="pf-c-nav__link pf-m-current" href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin/occupancy'}>Occupancy</a>
                    </Link>
                    <Link href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin/transmitters'} >
                      <a className="pf-c-nav__link" href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin/transmitters'}>Transmitters</a>
                    </Link>
                    <Link href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin/addService'} >
                      <a className="pf-c-nav__link" href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin/addService'}>Add Service</a>
                    </Link>
                </NavList>
              </Nav>
            </ToolbarItem>
          </ToolbarContent>
        </Toolbar>
  

        <PageSection isFilled key="multiplex_general">
            <p style={{"margin-bottom":"2px", "float":"right"}}>
              <Link href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin/addService'} >
                <a className="pf-link" href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin/addService'}>Add Service</a>
              </Link>
            </p>
           
            <TableComposable aria-label="Simple table" variant='compact' borders='compactBorderless'>
            <Thead>
              <Tr>
                <Th key="edit"></Th>
                <Th key="Broadcast Service">Broadcast Service</Th>
                <Th key="DSP License">DSP License</Th>
                <Th key="CommencementDate">Start</Th>
                <Th key="Termination Date">End</Th>
                <Th key="Sampling Rate">Sampling</Th>
                <Th key="Subtype">Type</Th>
                <Th key="Service Mode">Mode</Th>
                <Th key="Protection Level">Protection</Th>
                <Th key="CU">CU</Th>
                <Th key="PTY">PTY</Th>
                <Th key="SID">SID</Th>
                <Th key="IMG">Img</Th>
              </Tr>
            </Thead>
            <Tbody>
             {pageState.publicMultiplexData?.bearers && pageState?.publicMultiplexData?.bearers.map((svc, idx) => (
               <Tr key={svc.id}>
                 <Td>
                   <Link href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin/occupancy/'+svc.id} >
                     <a className="pf-link" href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin/occupancy/'+svc.id}>Manage</a>
                   </Link>
                 </Td>
                 <Td>{svc.service.name}</Td>
                 <Td>{svc.service.license}</Td>
                 <Td>{svc.startDate}</Td>
                 <Td>{svc.endDate}</Td>
                 <Td>{svc.samplingRate}</Td>
                 <Td>{svc.type}</Td>
                 <Td>{svc.mode}</Td>
                 <Td>{svc.protection}</Td>
                 <Td>{svc.capacityUnits}</Td>
                 <Td>{svc.parity}</Td>
                 <Td>{svc.service.sid}</Td>
                 <Td></Td>
               </Tr>
             ))}
            </Tbody>
          </TableComposable>
        </PageSection>
      </>
    );
  }
  else {
    return null;
  }
}

