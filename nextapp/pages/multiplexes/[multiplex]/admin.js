import React, { useEffect, useState } from "react";
import axios from 'axios'
import { TableComposable, Thead, Tbody, Tr, Th, Td } from '@patternfly/react-table';
import Link from 'next/link'
import { useRouter } from 'next/router'
import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionToggle,
  ActionGroup,
  Button,
  Card,
  CardHeader,
  CardActions,
  CardTitle,
  CardBody,
  DescriptionList,
  DescriptionListTerm,
  DescriptionListGroup,
  DescriptionListDescription,
  Form,
  FormGroup,
  FormSelect,
  FormSelectOption,
  Gallery,
  GalleryItem,
  Nav,
  NavItem,
  NavList,
  TextInput,
  TextArea,
  Toolbar,
  ToolbarContent,
  ToolbarItem,
  PageSection,
  PageSectionVariants,
  TextContent,
  Text,
} from '@patternfly/react-core';

import { useKeycloak } from '@react-keycloak/ssr';
import getConfig from "next/config";
import RefdataProperty from "../../../components/RefdataProperty";

const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();


export default function MultiplexAdmin() {

  const { keycloak, initialized } = useKeycloak()
  const user_data =  initialized && keycloak.authenticated ? keycloak.tokenParsed : null;
  const [pageState, setPageState] = useState({});
  const [userContextInfo, setUserContextInfo] = useState({});
  const router = useRouter()

  keycloak.updateToken(60);

  useEffect(() => {
    if ( initialized ) {
      console.log("Keycloak is initialised - will be sending %s as auth token",keycloak.token);
      const { multiplex } = router.query
      const fetchData = async () => {
        // Get multiplex using the secured API instead of the public one
        const result = await axios(publicRuntimeConfig.RR_API+'/multiplex/'+multiplex, 
                                   { 
                                     params: {setname:'full'} ,
                                     headers: { 'Authorization':'Bearer '+keycloak.token }
                                   } );
        // setPageState({...pageState, ['publicMultiplexData']: result.data});

        const regions = await axios(publicRuntimeConfig.RR_API+'/public/refdata/Region',
                                   {
                                     params: {} ,
                                     headers: { 'Authorization':'Bearer '+keycloak.token }
                                   } );

        // Computed property names - ES2015
        setPageState({...pageState, 
                      ['publicMultiplexData']: result.data,
                      ['regions']: regions.data.values});
        console.log("got regions: %o", regions);
      };

      fetchData();
    }
    else {
      console.log("waiting for kc initialisation");
    }
  }, [initialized, router.query]);

  const handleSubmit = (evt) => {
    console.log("Submit....%o",pageState.publicMultiplexData);
    evt.preventDefault();
    const postData = async() => {
      const result = await axios.put(publicRuntimeConfig.RR_API+'/multiplex/'+pageState.publicMultiplexData.id, 
                                     pageState.publicMultiplexData,
                                     { headers: { 'Authorization':'Bearer '+keycloak.token } } )
      // console.log("Result of put %s : %o",pageState.publicMultiplexData.id,result);
    }

    postData();
  };

  const handlePropertyChange = (prop, value) => {
    // this.setState({ value });
    var formData = pageState.publicMultiplexData
    formData[prop]=value
    setPageState({...pageState, ['publicMultiplexData']: formData});
  };


  // Active path can be found at router.pathname - style for active page should be className=pf-m-current

  if ( pageState.publicMultiplexData != null ) {
    return (
      <>
        <Toolbar id="AdminToolbar">
          <ToolbarContent>
            <ToolbarItem variant="label">
              Multiplex Admin: <a href={pageState?.publicMultiplexData?.homepage}>{pageState?.publicMultiplexData?.name}</a>
            </ToolbarItem>
            <ToolbarItem>
              <Nav variant="tertiary" theme="dark">
                <NavList>
                    <Link href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin'} >
                      <a className="pf-c-nav__link pf-m-current" href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin'}>General</a>
                    </Link>
                    <Link href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin/occupancy'} >
                      <a className="pf-c-nav__link" href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin/occupancy'}>Occupancy</a>
                    </Link>
                    <Link href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin/transmitters'} >
                      <a className="pf-c-nav__link" href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin/transmittersa'}>Transmitters</a>
                    </Link>
                    <Link href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin/addService'} >
                      <a className="pf-c-nav__link" href={"/multiplexes/"+pageState.publicMultiplexData.ead+'/admin/addService'}>Add Service</a>
                    </Link>
                </NavList>
              </Nav>
            </ToolbarItem>
          </ToolbarContent>
        </Toolbar>
  
        <PageSection isFilled key="multiplex_general">
          <Form>

            <Accordion asDefinitionList={false}>
              <AccordionItem>
                <AccordionToggle isExpanded={true}>General</AccordionToggle>
                <AccordionContent>
                  <Gallery hasGutter>
                    <GalleryItem>
                      <FormGroup label="Name" isRequired fieldId="horizontal-multiplex-name">
                        <TextInput value={pageState.publicMultiplexData.name} 
                                   isRequired 
                                   type="text" 
                                   onChange={(value) => handlePropertyChange('name',value)}
                                   id="horizontal-form-multiplex-name" 
                                   name="name" />
                      </FormGroup>
                    </GalleryItem>
                    <GalleryItem>
                      <FormGroup label="Ensemble ID" isRequired fieldId="horizontal-multiplex-eid">
                        <TextInput value={pageState.publicMultiplexData.ead} 
                                   isRequired 
                                   onChange={(value) => handlePropertyChange('ead',value)}
                                   type="text" 
                                   id="horizontal-form-multiplex-ead" 
                                   name="ead" />
                      </FormGroup>
                    </GalleryItem>
                    <GalleryItem>
                      <FormGroup label="Area"  fieldId="horizontal-multiplex-eid">
                        <TextInput value={pageState.publicMultiplexData.area}
                                   onChange={(value) => handlePropertyChange('area',value)}
                                   type="text"
                                   id="horizontal-form-multiplex-area"
                                   name="ead" />
                      </FormGroup>
                    </GalleryItem>
                    <GalleryItem>
                      <FormGroup label="Region"  fieldId="horizontal-multiplex-eid">
                        <RefdataProperty options={pageState.regions} value={pageState.publicMultiplexData.region} onSelect={(value) => handlePropertyChange('region',value)}/>
                      </FormGroup>
                    </GalleryItem>
                    <GalleryItem>
                      <FormGroup label="Homepage"  fieldId="horizontal-multiplex-homepage">
                        <TextInput value={pageState.publicMultiplexData.homepage} 
                                   onChange={(value) => handlePropertyChange('homepage',value)}
                                   type="text" 
                                   id="horizontal-form-multiplex-homepage" 
                                   name="homepage" />
                      </FormGroup>
                    </GalleryItem>

                    <GalleryItem>
                      <FormGroup label="Launch Date"  fieldId="horizontal-multiplex-launchDate">
                        <TextInput value={pageState.publicMultiplexData.launchDate} 
                                   onChange={(value) => handlePropertyChange('launchDate',value)}
                                   type="text" 
                                   id="horizontal-form-multiplex-launchDate" 
                                   name="launchDate" />
                      </FormGroup>
                    </GalleryItem>

                    <GalleryItem>
                      <FormGroup label="Logo URL"  fieldId="horizontal-multiplex-logo">
                        <TextInput value={pageState.publicMultiplexData.logoUrl}
                                   onChange={(value) => handlePropertyChange('logoUrl',value)}
                                   type="text"
                                   id="horizontal-form-multiplex-logoUrl"
                                   name="logoUrl" />
                      </FormGroup>
                    </GalleryItem>

                    <GalleryItem>
                      <FormGroup label="Classification"  fieldId="horizontal-multiplex-classn">
                        <TextInput value={pageState.publicMultiplexData.classn}
                                   onChange={(value) => handlePropertyChange('classn',value)}
                                   type="text"
                                   id="horizontal-form-multiplex-classn"
                                   name="classn" />
                      </FormGroup>
                    </GalleryItem>

                  </Gallery>

                  <FormGroup label="Description"  fieldId="horizontal-multiplex-Description">
                    <TextArea value={pageState.publicMultiplexData.description} 
                              onChange={(value) => handlePropertyChange('description',value)}
                              id="horizontal-form-multiplex-description" 
                              name="description" />
                  </FormGroup>

                </AccordionContent>
              </AccordionItem>


              <AccordionItem>
                <AccordionToggle isExpanded={true}>Technical Parameters</AccordionToggle>
                <AccordionContent>
                  <Gallery hasGutter>
                    <GalleryItem>
                      <FormGroup label="Block"  fieldId="horizontal-multiplex-block">
                        <TextInput value={pageState.publicMultiplexData.block}
                                   type="text"
                                   onChange={(value) => handlePropertyChange('block',value)}
                                   id="horizontal-form-multiplex-block"
                                   name="block" />
                      </FormGroup>
                    </GalleryItem>
                    <GalleryItem>
                      <FormGroup label="Frequency"  fieldId="horizontal-multiplex-frequency">
                        <TextInput value={pageState.publicMultiplexData.frequency}
                                   type="text"
                                   onChange={(value) => handlePropertyChange('frequency',value)}
                                   id="horizontal-form-multiplex-frequency"
                                   name="frequency" />
                      </FormGroup>
                    </GalleryItem>
                  </Gallery>

                </AccordionContent>
              </AccordionItem>

      
              <AccordionItem>
                <AccordionToggle isExpanded={true}>Registration Information and Licensee</AccordionToggle>
                <AccordionContent>
                  <Gallery hasGutter>
                    <GalleryItem>
                      <FormGroup label="License#" fieldId="horizontal-multiplex-license">
                        <TextInput value={pageState.publicMultiplexData.licenseNumber} type="text" id="horizontal-form-multiplex-licenseNumber"
                                   onChange={(value) => handlePropertyChange('licenseNumber',value)} name="licenseNumber" />
                      </FormGroup>
                    </GalleryItem>
                    <GalleryItem>
                      <FormGroup label="Licensee" fieldId="horizontal-multiplex-licensee">
                        <TextInput value={pageState.publicMultiplexData.licensee} type="text" id="horizontal-form-multiplex-licensee"
                                   onChange={(value) => handlePropertyChange('licensee',value)} name="licensee" />
                      </FormGroup>
                    </GalleryItem>
                    <GalleryItem>
                      <FormGroup label="Licensee Website"  fieldId="horizontal-multiplex-licensee-website">
                        <TextInput value={pageState.publicMultiplexData.licenseeWebsite}  type="text" id="horizontal-form-multiplex-licenseeWebsite"
                                   onChange={(value) => handlePropertyChange('licenseeWebsite',value)} name="licenseeWebsite" />
                      </FormGroup>
                    </GalleryItem>
                  </Gallery>
                </AccordionContent>
              </AccordionItem>
      
              <AccordionItem>
                <AccordionToggle isExpanded={true}>Address Information</AccordionToggle>
                <AccordionContent>
                  <Gallery hasGutter>
                    <GalleryItem>
                      <FormGroup label="Line 1"  fieldId="horizontal-multiplex-line1">
                        <TextInput value={pageState.publicMultiplexData.addrLine1}  type="text" id="horizontal-form-multiplex-addrLine1"
                                   onChange={(value) => handlePropertyChange('addrLine1',value)} name="addrLine1" />
                      </FormGroup>
                    </GalleryItem>
                    <GalleryItem>
                      <FormGroup label="Line 2"  fieldId="horizontal-multiplex-line2">
                        <TextInput value={pageState.publicMultiplexData.addrLine2}  type="text" id="horizontal-form-multiplex-addrLine2"
                                   onChange={(value) => handlePropertyChange('addrLine2',value)} name="addrLine2" />
                      </FormGroup>
                    </GalleryItem>
                    <GalleryItem>
                      <FormGroup label="Town"  fieldId="horizontal-multiplex-addrTown">
                        <TextInput value={pageState.publicMultiplexData.addrTown}  type="text" id="horizontal-form-multiplex-addrTown"
                                   onChange={(value) => handlePropertyChange('addrTown',value)} name="addrTown" />
                      </FormGroup>
                    </GalleryItem>
                    <GalleryItem>
                      <FormGroup label="Postcode"  fieldId="horizontal-multiplex-addrPostcode">
                        <TextInput value={pageState.publicMultiplexData.addrPostcode}  type="text" id="horizontal-form-multiplex-addrPostcode"
                                   onChange={(value) => handlePropertyChange('addrPostcode',value)} name="addrPostcode" />
                      </FormGroup>
                    </GalleryItem>
                    <GalleryItem>
                      <FormGroup label="Telephone"  fieldId="horizontal-multiplex-Telephone">
                        <TextInput value={pageState.publicMultiplexData.tel}  type="text" id="horizontal-form-multiplex-tel"
                                   onChange={(value) => handlePropertyChange('tel',value)} name="tel" />
                      </FormGroup>
                    </GalleryItem>
                    <GalleryItem>
                      <FormGroup label="Email"  fieldId="horizontal-multiplex-Email">
                        <TextInput value={pageState.publicMultiplexData.emailAddress}  type="text" id="horizontal-form-multiplex-emailAddress"
                                   onChange={(value) => handlePropertyChange('emailAddress',value)} name="emailAddress" />
                      </FormGroup>
                    </GalleryItem>
                  </Gallery>
                </AccordionContent>
              </AccordionItem>
      
              <AccordionItem>
                <AccordionToggle isExpanded={true}>Social Media</AccordionToggle>
                <AccordionContent>
                  <Gallery hasGutter>
                    <GalleryItem>
                      <FormGroup label="Twitter Handle"  fieldId="horizontal-multiplex-twitter">
                        <TextInput value={pageState.publicMultiplexData.twitterHandle}  type="text" id="horizontal-form-multiplex-twitterHandle"
                                   onChange={(value) => handlePropertyChange('twitterHandle',value)} name="twitterHandle" />
                      </FormGroup>
                    </GalleryItem>
                    <GalleryItem>
                      <FormGroup label="Facebook URL"  fieldId="horizontal-multiplex-fbpage">
                        <TextInput value={pageState.publicMultiplexData.fbPage}  type="text" id="horizontal-form-multiplex-fbPage"
                                   onChange={(value) => handlePropertyChange('fbPage',value)} name="fbPage" />
                      </FormGroup>
                    </GalleryItem>
                    <GalleryItem>
                      <FormGroup label="Instagram URL"  fieldId="horizontal-multiplex-instagramAddr">
                        <TextInput value={pageState.publicMultiplexData.instagramAddr}  type="text" id="horizontal-form-multiplex-instagramAddr"
                                   onChange={(value) => handlePropertyChange('instagramAddr',value)} name="instagramAddr" />
                      </FormGroup>
                    </GalleryItem>
                    <GalleryItem>
                      <FormGroup label="Youtube Channel URL"  fieldId="horizontal-multiplex-youtubeChannel">
                        <TextInput value={pageState.publicMultiplexData.youtubeChannel}  type="text" id="horizontal-form-multiplex-youtubeChannel"
                                   onChange={(value) => handlePropertyChange('youtubeChannel',value)} name="youtubeChannel" />
                      </FormGroup>
                    </GalleryItem>
                  </Gallery>
                </AccordionContent>
              </AccordionItem>
            </Accordion>

            <ActionGroup>
              <Button variant="primary" onClick={handleSubmit}>Submit</Button>
              <Button variant="link">Cancel</Button>
            </ActionGroup>

          </Form>
        </PageSection>
      </>
    );
  }
  else {
    return null;
  }
}

