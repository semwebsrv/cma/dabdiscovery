import React, { useEffect, useState } from "react";
import axios from 'axios'
import { TableComposable, Thead, Tbody, Tr, Th, Td } from '@patternfly/react-table';
import {
  Button,
  Form,
  FormGroup,
  InputGroup,
  Pagination,
  TextInput,
} from '@patternfly/react-core';
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useKeycloak } from '@react-keycloak/ssr';

import getConfig from "next/config";
const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();

export default function Multiplexes(props) {

  const router = useRouter()
  const [qr, setQr] = useState(props.initialMultiplexes);
  const [pageNumber, setPageNumber] = useState(1);
  const [queryString, setQueryString] = useState('');
  const [textInput, setTextInput] = useState(router.query.q);

  const { keycloak, initialized } = useKeycloak()
  const user_data = initialized && keycloak.authenticated ? keycloak.tokenParsed : null;
  const is_admin = user_data?.realm_access?.roles?.includes('ADMIN')
  const is_accredited = user_data?.realm_access?.roles?.includes('ACCREDITED')


  useEffect(() => {
    const fetchData = async () => {
      const query = ( ( queryString != null ) && ( queryString.length > 0 ) ) ? queryString : '%';
      const offset = ( pageNumber - 1 ) * 10
      const url_endpoint = publicRuntimeConfig.RR_API+'/public/multiplexes';
      console.log("Request multiplexes from : %s",url_endpoint);
      const result = await axios(url_endpoint, { params: {q:query, max:10, offset:offset, sort:'name' } } );
      setQr(result.data);
    };
 
    fetchData();
  }, [queryString, pageNumber]);

  const onSetPage = (event, n) => {
    console.log("set page %o",n);
    setPageNumber(n)
  }

  const onPerPageSelect = (n) => {
  }

  const handleQuery = (event) => {
    router.push('/multiplexes', '/multiplexes?q='+textInput, { shallow: true })
    setQueryString(textInput);
    setPageNumber(1);
    event.preventDefault();
  }

  const handleTextChange = (n) => {
    setTextInput(n);
  }

  const goToCreateMultiplex = () => {
    router.push("/multiplexes/admin/create");
  }

  return (
    <div>
      <Form onSubmit={handleQuery} isHorizontal>
        <FormGroup label="Multiplex name:" fieldId="multiplex-name">
          <InputGroup>
            <TextInput id="qry"
                       isRequired type="text"
                       name="simple-form-name-01"
                       aria-describedby="simple-form-name-01-helper"
                       onChange={handleTextChange}
                       value={textInput} />
            <Button type="submit" variant="primary">Search</Button>
            {
              ( ( is_admin || is_accredited ) && (
                <span>&nbsp; <Button type="submit" onClick={goToCreateMultiplex} variant="primary">New</Button></span>
              ) )
            }
          </InputGroup>
        </FormGroup>
        
      </Form>
 
      <Pagination
        itemCount={qr?.totalCount}
        perPage={10}
        page={pageNumber}
        onSetPage={onSetPage}
        widgetId="pagination-options-menu-top"
        onPerPageSelect={onPerPageSelect}
      />

      <TableComposable aria-label="A list of DAB multiplexes">
        <Thead>
          <Tr>
            <Th>Multiplex name</Th>
            <Th>Ensemble Id</Th>
            <Th>Ensemble Type</Th>
            <Th>Multiplex Area</Th>
            <Th>Region</Th>
          </Tr>
        </Thead>
        <Tbody>
          {qr.resultList && qr.resultList.map((row, rowIndex) => (
            <Tr key={'rr_mp_'+row.ead}>
              <Td><Link href={"/multiplexes/"+row.ead}>{row.name}</Link></Td>
              <Td>{row.ead}</Td>
              <Td>{row.classn}</Td>
              <Td>{row.area}</Td>
              <Td>{row.region?.label}</Td>
            </Tr>
          ))}
        </Tbody>
      </TableComposable>
    </div>
  )
}

export async function getServerSideProps(context) {
  const endpoint_url = publicRuntimeConfig.RR_API+'/public/multiplexes?q=%25&max=10&offset=0&sort=name'
  console.log("serverSide fetch multiplexes: %s",endpoint_url);
  const res = await fetch(endpoint_url)
  const data = await res.json()
  return { props: { initialMultiplexes: data } }
}

