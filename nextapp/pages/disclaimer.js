import React, { useEffect, useState } from "react";
import axios from 'axios'

export default function Home() {

  return (
    <div>
      <h1>Disclaimer</h1>
      <p>
While we have made every attempt to ensure that the information contained in this Site has been obtained from reliable sources, the Community Media Association is not responsible for any errors or omissions, or for the results obtained from the use of this information. All information in this site is provided "as is", with no guarantee of completeness, accuracy, timeliness or of the results obtained from the use of this information, and without warranty of any kind, express or implied, including, but not limited to warranties of performance, merchantability and fitness for a particular purpose. In no event will the Community Media Association, its related partnerships or corporations, or the partners, agents or employees thereof be liable to you or anyone else for any decision made or action taken in reliance on the information in this site or for any consequential, special or similar damages, even if advised of the possibility of such damages.
      </p>
    </div>
  )
}
