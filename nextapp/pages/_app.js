import '@patternfly/react-core/dist/styles/base.css'
import cookie from 'cookie'
import { SSRKeycloakProvider, SSRCookies, useKeycloak } from '@react-keycloak/ssr';
import React, { useEffect, useState } from "react";
import UserMenu from '../components/UserMenu'
import MainLayout from '../components/MainLayout'
import getConfig from "next/config";
const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();


const keycloakCfg = {
  //realm: 'Semweb',
  realm: 'CMASelfService',
  url: 'https://keycloak.semweb.co/auth/',
  clientId: 'selfservice'
}

// https://www.keycloak.org/docs/latest/securing_apps/index.html#init-options
// onLoad: 'login-required' - require a login
// onLoad: 'check-sso' - check to see if there is a user login
const initOptions = {
  onLoad: 'check-sso',
  // Cant find a nice way to make this work atm
  // silentCheckSsoRedirectUri: window.location.origin + '/silent-check-sso.html'
}

function MyApp({ Component, pageProps, cookies }) {

  const { keycloak, initialized } = useKeycloak()

  // console.log("NEXT_PUBLIC_ENV_LABEL: %s",process.env.NEXT_PUBLIC_ENV_LABEL);
  // console.log("NEXT_PUBLIC_BUILDNO: %s",process.env.NEXT_PUBLIC_BUILDNO);

  // Working from https://www.saltycrane.com/blog/2021/04/buildtime-vs-runtime-environment-variables-nextjs-docker/
  // console.log("publicRuntimeConfig.RR_API: %s",publicRuntimeConfig.RR_API);

  return (
    <SSRKeycloakProvider keycloakConfig={keycloakCfg} persistor={SSRCookies(cookies)} initOptions={initOptions} >
      <MainLayout>
        <Component {...pageProps} />
      </MainLayout>
    </SSRKeycloakProvider>
  )
}

function parseCookies(req) {
  if (!req || !req.headers) {
    return {}
  }
  return cookie.parse(req.headers.cookie || '')
}

MyApp.getInitialProps = async ({Component, context}) => {
  return { 
    cookies: parseCookies(context?.ctx?.req) 
  }
}

export default MyApp
