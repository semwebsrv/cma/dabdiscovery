import React, { useEffect, useState } from "react";
import axios from 'axios'
import { TableComposable, Thead, Tbody, Tr, Th, Td } from '@patternfly/react-table';
import Link from 'next/link'
import { useRouter } from 'next/router'
import {
  Card,
  CardHeader,
  CardActions,
  CardTitle,
  CardBody,
  DescriptionList,
  DescriptionListTerm,
  DescriptionListGroup,
  DescriptionListDescription,
  Gallery,
  PageSection,
  PageSectionVariants,
  TextContent,
  Text,
} from '@patternfly/react-core';
import { useKeycloak } from '@react-keycloak/ssr';
import OwnershipInfo from '../../components/OwnershipInfo'
import getConfig from "next/config";
const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();

export default function Service() {

  const { keycloak, initialized } = useKeycloak()
  const user_data =  initialized && keycloak.authenticated ? keycloak.tokenParsed : null;
  const [qr, setQr] = useState({});
  const router = useRouter()

  useEffect(() => {
    const { service } = router.query
    const fetchData = async () => {
      const result = await axios(publicRuntimeConfig.RR_API+'/public/services/'+service, { params: {setname:'full'} } );
      setQr(result.data);
    };

    fetchData();
  }, []);

  // Inspiration from https://www.patternfly.org/v4/demos/card-view
  // <Toolbar id="toolbar-group-types" clearAllFilters={this.onDelete}>
  // <ToolbarContent>{toolbarItems}</ToolbarContent>
  // </Toolbar>
  // <img src={qr.logo128} style={{float:'right', overflow:'auto'}}/>

  return (
    <>
      <PageSection variant={PageSectionVariants.light}>
        <img src={qr.logo128} style={{float:'right', overflow:'auto', padding:'2px'}}/>
        <TextContent>
          <div>
            <Text component="h1">Broadcast service: {qr.name}</Text>
            <Text component="p">{qr.description}</Text>
            <OwnershipInfo name={qr.name} owner={qr.owner} type="Broadcast Service" resourceId={qr.id} userData={user_data}/>
          </div>
        </TextContent>
      </PageSection>

      <PageSection isFilled>
        <DescriptionList>

          <DescriptionListGroup>
            <DescriptionListTerm>Name</DescriptionListTerm>
            <DescriptionListDescription>{qr.name}</DescriptionListDescription>
          </DescriptionListGroup>

          <DescriptionListGroup>
            <DescriptionListTerm>Bitrate</DescriptionListTerm>
            <DescriptionListDescription>{qr.bitrate}</DescriptionListDescription>
          </DescriptionListGroup>

          <DescriptionListGroup>
            <DescriptionListTerm>Codec</DescriptionListTerm>
            <DescriptionListDescription>{qr.codec}</DescriptionListDescription>
          </DescriptionListGroup>

          <DescriptionListGroup>
            <DescriptionListTerm>Mono/Stereo</DescriptionListTerm>
            <DescriptionListDescription>{qr.channelType}</DescriptionListDescription>
          </DescriptionListGroup>

          <DescriptionListGroup>
            <DescriptionListTerm>Slideshow</DescriptionListTerm>
            <DescriptionListDescription>{qr.hasSlideshow}</DescriptionListDescription>
          </DescriptionListGroup>

          <DescriptionListGroup>
            <DescriptionListTerm>Genre</DescriptionListTerm>
            <DescriptionListDescription>{qr.genre}</DescriptionListDescription>
          </DescriptionListGroup>

          <DescriptionListGroup>
            <DescriptionListTerm>DAB multiplexes carrying this service</DescriptionListTerm>
            <DescriptionListDescription>
              <Gallery hasGutter>
                {qr.bearers && qr.bearers.map((b, idx) => (
                  <Card isHoverable isCompact key={b.multiplex.name}>
                    <CardTitle><Link href={"/multiplexes/"+b.multiplex.ead}>{b.multiplex.name}</Link></CardTitle>
                    <CardBody>

                    </CardBody>
                  </Card>
                ))}
              </Gallery>
            </DescriptionListDescription>
          </DescriptionListGroup>

        </DescriptionList>
      </PageSection>
    </>
  )
  // Date ranges for bearers can be shown with Between {b.from} and {b.to}
}
