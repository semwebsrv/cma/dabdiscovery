import React, { useEffect, useState } from "react";
import axios from 'axios'
import { TableComposable, Thead, Tbody, Tr, Th, Td } from '@patternfly/react-table';
import Link from 'next/link'
import { useRouter } from 'next/router'
import {
  ActionGroup,
  Button,
  Card,
  CardHeader,
  CardActions,
  CardTitle,
  CardBody,
  DescriptionList,
  DescriptionListTerm,
  DescriptionListGroup,
  DescriptionListDescription,
  Form,
  FormGroup,
  Gallery,
  PageSection,
  PageSectionVariants,
  Select,
  SelectOption,
  SelectVariant,
  Text,
  TextContent,
  TextInput,
  Title,
} from '@patternfly/react-core';
import { useKeycloak } from '@react-keycloak/ssr';
import getConfig from "next/config";
const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();

export default function Service() {

  const [pageState, setPageState] = useState({newServiceData:{}});
  const { keycloak, initialized } = useKeycloak()
  const user_data =  initialized && keycloak.authenticated ? keycloak.tokenParsed : null;
  const [qr, setQr] = useState({});
  const router = useRouter()
  const [ options, setOptions ] = useState([]);
  const [ isPartyTypedownOpen, setIsPartyTypedownOpen ] = useState(false);
  const [ selectedParty, setSelectedParty ] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      const qr = await axios(publicRuntimeConfig.RR_API+'/public/parties',
                                 {
                                   params: {
                                     setname:'brief',
                                     q:'%',
                                     max:1000,
                                     offset:0
                                   }
                                 });
      console.log("Got filter result %o",qr);
      if ( qr && qr.data && qr.data.resultList) {
        const option_data = qr.data.resultList.map ( ( p, index ) => {
          return {
            id: p.id,
            value: {
              resource: p,
              toString: () => { return p.name }
            }
          }
        });
        setOptions(option_data);
        // setIsPartyTypedownOpen(true)
      }
    }
    fetchData();
  }, []);


  // Active path can be found at router.pathname - style for active page should be className=pf-m-current
  const handleTextInputChange = (prop, value) => {
    var formData = pageState.newServiceData
    formData[prop]=value
    setPageState({...pageState, ['newServiceData']: formData});
  };

  const handleSubmit = (evt) => {
    console.log("Submit....%o",pageState.newDABBearerData);

    evt.preventDefault();
    const postData = async() => {
      const result = await axios.post(publicRuntimeConfig.RR_API+'/broadcastService',
                                     pageState.newServiceData,
                                     { headers: { 'Authorization':'Bearer '+keycloak.token } } )

      console.log("result: %o",result);
      if ( result.data.id ) {
        router.push("/services/"+result.data.slug);
      }
    }

    postData();
  };


  const onToggle = () => {
    setIsPartyTypedownOpen( !isPartyTypedownOpen )
  }

  const onSelect = (event, selection, isPlaceholder) => {
    console.log("onSelect %o",selection);
    if ( ( selection != null ) &&
         ( selection.resource?.id != null ) ) {
      setSelectedParty(selection);
      var formData = pageState.newServiceData
      formData.owner=selection.resource
      setPageState({...pageState, ['newServiceData']: formData});
      setIsPartyTypedownOpen(false)
    }
  }

  const clearSelection = () => {
    setSelectedParty(null);
  }

  const onCreateParty = (newOptionValue) => {
    // console.log("createparty %s",newOptionValue)
    setSelectedParty({
      resource:{
        id:null,
        name:newOptionValue
      },
      toString: () => { return newOptionValue }
    });
  }


  return (
    <PageSection isFilled key="create_service">
      <Title headingLevel="h1">Create New Broadcast Service</Title>
      <Form>
        <DescriptionList>

          <DescriptionListGroup key="create_service_group">

            <DescriptionListDescription>
              <p>Select the organisation owning this data, or enter a name to create a new authority record. If you
                   are acting as a private citizen, please a name of your choosing here. Note that the name provided
                   will be visible to all site users as the party responsible for the data maintained</p>
              <Select
                  variant={SelectVariant.typeahead}
                  typeAheadAriaLabel="Organisation Name"
                  onToggle={onToggle}
                  onSelect={onSelect}
                  onClear={clearSelection}
                  selections={selectedParty}
                  isOpen={isPartyTypedownOpen}
                  aria-labelledby="party-typedown-1"
                  isInputValuePersisted={true}
                  placeholderText="Enter your organisation name"
                  isDisabled={false}
                  isCreatable={true}
                  onCreateOption={onCreateParty}
              >
                  {options.map((option, index) => (
                    <SelectOption key={option.id} value={option.value} index={index} />
                  ))}
              </Select>
            </DescriptionListDescription>

            <DescriptionListTerm>Service ID (SID)</DescriptionListTerm>
            <DescriptionListDescription>
              <TextInput id="ti-svc-sid" onChange={(value) => handleTextInputChange('sid',value)}/>
            </DescriptionListDescription>

            <DescriptionListTerm>Service Name</DescriptionListTerm>
            <DescriptionListDescription>
              <TextInput id="ti-svc-name" onChange={(value) => handleTextInputChange('name',value)}/>
            </DescriptionListDescription>

            <DescriptionListTerm>Bitrate</DescriptionListTerm>
            <DescriptionListDescription>
              <TextInput id="ti-svc-bitrate" onChange={(value) => handleTextInputChange('bitrate',value)}/>
            </DescriptionListDescription>

            <DescriptionListTerm>Codec</DescriptionListTerm>
            <DescriptionListDescription>
              <TextInput id="ti-svc-codec" onChange={(value) => handleTextInputChange('codec',value)}/>
            </DescriptionListDescription>

            <DescriptionListTerm>Channel Type</DescriptionListTerm>
            <DescriptionListDescription>
              <TextInput id="ti-svc-channelType" onChange={(value) => handleTextInputChange('channelType',value)}/>
            </DescriptionListDescription>

            <DescriptionListTerm>Genre</DescriptionListTerm>
            <DescriptionListDescription>
              <TextInput id="ti-svc-genre" onChange={(value) => handleTextInputChange('genre',value)}/>
            </DescriptionListDescription>

            <DescriptionListTerm>Slideshow</DescriptionListTerm>
            <DescriptionListDescription>
              <TextInput id="ti-svc-slideshow" onChange={(value) => handleTextInputChange('slideshow',value)}/>
            </DescriptionListDescription>


          </DescriptionListGroup>

        </DescriptionList>

        <ActionGroup>
          <Button variant="primary" onClick={handleSubmit}>Submit</Button>
          <Button variant="link">Cancel</Button>
        </ActionGroup>
      </Form>
    </PageSection>
  )
}
