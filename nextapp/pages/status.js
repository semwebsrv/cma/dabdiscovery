import React, { useEffect, useState } from "react";

import { useKeycloak } from '@react-keycloak/ssr';

export default function Status(props) {

  const { keycloak, initialized } = useKeycloak()
  const user_data =  initialized && keycloak.authenticated ? keycloak.tokenParsed : null;

  return (
    <div>
      <ul>
        <li>Keycloak Initialised: {initialized ? 'true' : 'false'}</li>
        <li>Keycloak Login Url: {initialized && keycloak && ( keycloak.createLoginUrl != null ) && keycloak.createLoginUrl()} </li>
        <li>User Data: {JSON.stringify(user_data)}</li>
      </ul>
    </div>
  )
}
