import React, { useEffect, useState } from "react";
import axios from 'axios'
import { TableComposable, Thead, Tbody, Tr, Th, Td } from '@patternfly/react-table';
import Link from 'next/link'
import { useRouter } from 'next/router'
import {
  ActionGroup,
  Button,
  Card,
  CardHeader,
  CardActions,
  CardTitle,
  CardBody,
  DescriptionList,
  DescriptionListTerm,
  DescriptionListGroup,
  DescriptionListDescription,
  Form,
  FormGroup,
  Gallery,
  PageSection,
  PageSectionVariants,
  Select,
  SelectOption,
  SelectVariant,
  Text,
  TextContent,
  TextInput,
  Title,
} from '@patternfly/react-core';
import { useKeycloak } from '@react-keycloak/ssr';

import getConfig from "next/config";
const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();

export default function NewTransmitter() {

  const [pageState, setPageState] = useState({newTransmitterData:{}});
  const { keycloak, initialized } = useKeycloak()
  const user_data =  initialized && keycloak.authenticated ? keycloak.tokenParsed : null;
  const [qr, setQr] = useState({});
  const router = useRouter()
  const [ options, setOptions ] = useState([]);
  const [ isPartyTypedownOpen, setIsPartyTypedownOpen ] = useState(false);
  const [ selectedParty, setSelectedParty ] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      const qr = await axios(publicRuntimeConfig.RR_API+'/public/parties',
                                 {
                                   params: {
                                     setname:'brief',
                                     q:'%',
                                     max:1000,
                                     offset:0
                                   }
                                 });
      console.log("Got filter result %o",qr);
      if ( qr && qr.data && qr.data.resultList) {
        const option_data = qr.data.resultList.map ( ( p, index ) => {
          return {
            id: p.id,
            value: {
              resource: p,
              toString: () => { return p.name }
            }
          }
        });
        setOptions(option_data);
        // setIsPartyTypedownOpen(true)
      }
    }
    fetchData();
  }, []);


  // Active path can be found at router.pathname - style for active page should be className=pf-m-current
  const handleTextInputChange = (prop, value) => {
    var formData = pageState.newTransmitterData
    formData[prop]=value
    setPageState({...pageState, ['newTransmitterData']: formData});
  };

  const handleSubmit = (evt) => {
    console.log("Submit....%o",pageState.newDABBearerData);

    evt.preventDefault();
    const postData = async() => {
      const result = await axios.post(publicRuntimeConfig.RR_API+'/transmitter',
                                     pageState.newTransmitterData,
                                     { headers: { 'Authorization':'Bearer '+keycloak.token } } )

      console.log("result: %o",result);
      if ( result.data.id ) {
        router.push("/transmitters/"+result.data.slug);
      }
    }

    postData();
  };


  const onToggle = () => {
    setIsPartyTypedownOpen( !isPartyTypedownOpen )
  }

  const onSelect = (event, selection, isPlaceholder) => {
    console.log("onSelect %o",selection);
    if ( ( selection != null ) &&
         ( selection.resource?.id != null ) ) {
      setSelectedParty(selection);
      var formData = pageState.newTransmitterData
      formData.owner=selection.resource
      setPageState({...pageState, ['newTransmitterData']: formData});
      setIsPartyTypedownOpen(false)
    }
  }

  const clearSelection = () => {
    setSelectedParty(null);
  }

  const onCreateParty = (newOptionValue) => {
    // console.log("createparty %s",newOptionValue)
    setSelectedParty({
      resource:{
        id:null,
        name:newOptionValue
      },
      toString: () => { return newOptionValue }
    });
  }


  return (
    <PageSection isFilled key="create_transmitter">
      <Title headingLevel="h1">Create New Transmitter</Title>
      <Form>
        <DescriptionList>

          <DescriptionListGroup key="create_transmitter_group">

            <DescriptionListDescription>
              <p>Select the organisation owning this data, or enter a name to create a new authority record. If you
                   are acting as a private citizen, please a name of your choosing here. Note that the name provided
                   will be visible to all site users as the party responsible for the data</p>
              <Select
                  variant={SelectVariant.typeahead}
                  typeAheadAriaLabel="Organisation Name"
                  onToggle={onToggle}
                  onSelect={onSelect}
                  onClear={clearSelection}
                  selections={selectedParty}
                  isOpen={isPartyTypedownOpen}
                  aria-labelledby="party-typedown-1"
                  isInputValuePersisted={true}
                  placeholderText="Enter your organisation name"
                  isDisabled={false}
                  isCreatable={true}
                  onCreateOption={onCreateParty}
              >
                  {options.map((option, index) => (
                    <SelectOption key={option.id} value={option.value} index={index} />
                  ))}
              </Select>
            </DescriptionListDescription>

            <DescriptionListGroup>
              <DescriptionListTerm>Name</DescriptionListTerm>
              <DescriptionListDescription>
                <TextInput id="ti-transmitter-name" onChange={(value) => handleTextInputChange('name',value)}/>
              </DescriptionListDescription>
            </DescriptionListGroup>

            <DescriptionListGroup>
              <DescriptionListTerm>Location Reference (E.G. OSGB36:....)</DescriptionListTerm>
              <DescriptionListDescription>
                <TextInput id="ti-transmitter-lcsRef" onChange={(value) => handleTextInputChange('lcsRef',value)}/>
              </DescriptionListDescription>
            </DescriptionListGroup>



          </DescriptionListGroup>

        </DescriptionList>

        <ActionGroup>
          <Button variant="primary" onClick={handleSubmit}>Submit</Button>
          <Button variant="link">Cancel</Button>
        </ActionGroup>
      </Form>
    </PageSection>
  )
}
