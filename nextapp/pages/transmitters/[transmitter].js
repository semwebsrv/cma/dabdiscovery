import React, { useEffect, useState } from "react";
import axios from 'axios'
import { TableComposable, Thead, Tbody, Tr, Th, Td } from '@patternfly/react-table';
import Link from 'next/link'
import { useRouter } from 'next/router'
import {
  Card,
  CardHeader,
  CardActions,
  CardTitle,
  CardBody,
  DescriptionList,
  DescriptionListTerm,
  DescriptionListGroup,
  DescriptionListDescription,
  Gallery,
  PageSection,
  PageSectionVariants,
  TextContent,
  Text,
} from '@patternfly/react-core';
import { useKeycloak } from '@react-keycloak/ssr';
import OwnershipInfo from '../../components/OwnershipInfo'

import getConfig from "next/config";
const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();

export default function Transmitter() {

  const { keycloak, initialized } = useKeycloak()
  const user_data =  initialized && keycloak.authenticated ? keycloak.tokenParsed : null;
  const [qr, setQr] = useState({});
  const router = useRouter()

  useEffect(() => {
    const { transmitter } = router.query
    const fetchData = async () => {
      const result = await axios(publicRuntimeConfig.RR_API+'/public/transmitters/'+transmitter, { params: {setname:'full'} } );
      setQr(result.data);
    };

    fetchData();
  }, []);

  return (
    <>
      <PageSection variant={PageSectionVariants.light}>
        <TextContent>
          <Text component="h1">Transmitter: {qr.name}</Text>
          <OwnershipInfo name={qr.name} owner={qr.owner} type="Transmitter" resourceId={qr.id} userData={user_data} />
        </TextContent>
      </PageSection>

      <PageSection isFilled>
        <DescriptionList>
          <DescriptionListGroup>
            <DescriptionListTerm>Name</DescriptionListTerm>
            <DescriptionListDescription>{qr.name}</DescriptionListDescription>
          </DescriptionListGroup>

          <DescriptionListGroup>
            <DescriptionListTerm>Multiplexes operating in this area</DescriptionListTerm>
            <DescriptionListDescription>
              <Gallery hasGutter>
                {qr.multiplexes && qr.multiplexes.map((mtr, idx) => (
                  <Card isHoverable isCompact key={mtr.multiplex.name}>
                    <CardTitle><Link href={"/multiplexes/"+mtr.multiplex.ead}>{mtr.multiplex.name}</Link></CardTitle>
                    <CardBody>multiplex info</CardBody>
                  </Card>
                ))}
              </Gallery>
            </DescriptionListDescription>
          </DescriptionListGroup>

        </DescriptionList>
      </PageSection>
    </>
  )
}
