import React, { useEffect, useState } from "react";

import { Checkbox, Select, SelectOption, SelectVariant } from '@patternfly/react-core';

function RefdataProperty({titleId, options, value, onSelect}) {

  const [ isOpen, setIsOpen ] = useState(false);
  const [ selected, setSelected ] = useState(null);

  const handleSelectEvent = (event, selection, isPlaceholder) => {
    if (isPlaceholder) 
      clearSelection();
    else {
      setSelected(selection)
      setIsOpen(false)
      console.log('selected:', selection);
      if ( onSelect ) {
        onSelect(selection);
      }
    }
  }

  const onToggle = isOpen => {
    setIsOpen(isOpen);
  };

  const clearSelection = () => {
  }

  const createOption = (row) => {
    return {
      id: row.id,
      value: row.value,
      label: row.label,
      toString: function(value) {
        return this.label;
      }
    }
  }

  const select_options = options.map((option, index) => (
    <SelectOption
          key={option.value}
          value={createOption(option)}/>
  ));


  return (
    <Select
      variant={SelectVariant.typeahead}
      typeAheadAriaLabel="Select a state"
      onToggle={onToggle}
      onSelect={handleSelectEvent}
      // onClear={this.clearSelection}
      selections={selected}
      isOpen={isOpen}
      aria-labelledby={titleId}
      // isInputValuePersisted={isInputValuePersisted}
      // isInputFilterPersisted={isInputFilterPersisted}
      placeholderText="Select a state"
      // isDisabled={isDisabled}
      isCreatable={false}
      // onCreateOption={(hasOnCreateOption && this.onCreateOption) || undefined}
    >
      {select_options}
    </Select>
  );
}

export default RefdataProperty;
