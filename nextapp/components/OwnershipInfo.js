import React, { useEffect, useState } from "react";
import { useKeycloak } from '@react-keycloak/ssr';

import {
  PageSection,
  PageSectionVariants,
  TextContent,
  Text,
  TextArea,
  Select,
  SelectOption,
  SelectVariant,
  DescriptionList,
  DescriptionListTerm,
  DescriptionListGroup,
  DescriptionListDescription,
} from '@patternfly/react-core';
import Link from 'next/link'
import { Modal, ModalVariant, Button, Wizard } from '@patternfly/react-core';
import axios from 'axios'

import getConfig from "next/config";
const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();


function OwnershipInfo({name, owner, type, resourceId, userData, lastUpdated}) {

  const user_is_owner = false;
  const [ isModalOpen, setIsModalOpen ] = useState(false);
  const [ isPartyTypedownOpen, setIsPartyTypedownOpen ] = useState(false);
  const [ selectedParty, setSelectedParty ] = useState(null);
  const [ options, setOptions ] = useState([]);
  const [ requestNote, setRequestNote ] = useState('');

  const toggleAdoptThisRecord = () => {
    console.log("toggleAdoptThisRecord",isModalOpen);
    setIsModalOpen(!isModalOpen)
  }

  const postAdoptRequest = () => {
    console.log("Submit request for %o to access %o with note %s",userData,selectedParty,requestNote);
    axios(publicRuntimeConfig.RR_API+'/selfService/requestAccess', {
             params:{
               resourceType:type,
               resourceId:resourceId,
               resourceLabel:name,
               authorityName:selectedParty?.resource?.name,
               authorityId:selectedParty?.resource?.id,
               requestedPerm:'EDITOR',
               message:requestNote
             }}).
    then ( (result) => {
      console.log("got result: %o",result);
      setIsModalOpen(false);
    })
  }
  
  const closeAdoptModal = () => {
    setIsModalOpen(false);
  }

  // Populate the dropdown of orgs - probably better to ise onTypeaheadInputChanged(event, value) to load a filtered set
  // of org names - this will do for now tho
  useEffect(() => {
    const fetchData = async () => {
      const qr = await axios(publicRuntimeConfig.RR_API+'/public/parties',
                                 { 
                                   params: {
                                     setname:'brief',
                                     q:'%',
                                     max:1000,
                                     offset:0
                                   } 
                                 });
      console.log("Got filter result %o",qr);
      if ( qr && qr.data && qr.data.resultList) {
        const option_data = qr.data.resultList.map ( ( p, index ) => {
          return {
            id: p.id,
            value: {
              resource: p,
              toString: () => { return p.name }
            }
          }
        });
        setOptions(option_data);
        setIsPartyTypedownOpen(true)
      }
    }
    fetchData();
  }, []);

  const renderAdoptText = () => {
        if ( user_is_owner ) {
          return <Text component="p">
            You are registered as a maintainer of this record. Last updated {lastUpdated}
          </Text>
        }
        else if ( owner == null || owner?.name=='System' ) {
          if ( userData != null ) {
            return <Text component="p">
              This {type} record is up for adoption. If you are a responsible party, you can request maintainer status.<br/>
               <Button variant="primary" onClick={toggleAdoptThisRecord}>Adopt this record</Button> &nbsp;
               Last updated {lastUpdated}
            </Text>
          }
          else {
            return <Text component="p">
              This {type} record is up for adoption. If you are a responsible party, please log in and request permission 
              to update this record to help us build and maintain the most accurate picture of small scale radio services in the UK. &nbsp;
              Last updated {lastUpdated}
            </Text>
          }
        }
        else {
          return <Text component="p">
            This {type} record is maintained by <Link href={"/authorities/"+owner.shortcode}>{owner.name}</Link> &nbsp;
            and was last updated at {lastUpdated}
          </Text>
        }
  }

  const onToggle = () => {
    setIsPartyTypedownOpen( !isPartyTypedownOpen )
  }

  const onSelect = (event, selection, isPlaceholder) => {
    console.log("onSelect %o",selection);
    if ( ( selection != null ) &&
         ( selection.resource?.id != null ) ) {
      setSelectedParty(selection);
      setIsPartyTypedownOpen(false)
    }
  }

  const clearSelection = () => {
    setSelectedParty(null);
  }

  const updateInfo = (n) => {
    setRequestNote(n)
  }

  const onCreateParty = (newOptionValue) => {
    // console.log("createparty %s",newOptionValue)
    setSelectedParty({
      resource:{
        id:null,
        name:newOptionValue
      },
      toString: () => { return newOptionValue }
    });
  }


  const adopt_modal = userData && (
      <Modal
          variant={ModalVariant.medium}
          title="Adopt Record"
          isOpen={isModalOpen}
          onClose={toggleAdoptThisRecord}
          actions={[
            <Button key="confirm" 
                    variant="primary" 
                    isDisabled={ ( selectedParty == null) || ( selectedParty=='' ) }
                    onClick={postAdoptRequest}>
              Confirm
            </Button>,
            <Button key="cancel" variant="link" onClick={closeAdoptModal}>
              Cancel
            </Button>
          ]}
        >
          <DescriptionList>
            <DescriptionListGroup key="grant_basis_group">
              <DescriptionListTerm>Preamble</DescriptionListTerm>
              <DescriptionListDescription>
                <Text>Our goal is to simplify the process of transmitting data to OFCOM.
                   Because we want radio registry to be accurate
                   we need to ensure that users updating records have the proper 
                   authority to do so. Please use this form to tell us why <strong>{userData.name}</strong> is allowed to update 
                   the {type} record "<strong>{name}</strong>".
                   You should check with the data controller that you have permission to edit this data.
                </Text>
              </DescriptionListDescription>
              <DescriptionListTerm>Your right to edit this data</DescriptionListTerm>
              <DescriptionListDescription>
                <p>Select the organisation owning this data, or enter a name to create a new authority record. If you
                   are acting as a private citizen, please a name of your choosing here. Note that the name provided
                   will be visible to all site users as the party responsible for the data maintained</p>
                <Select
                  variant={SelectVariant.typeahead}
                  typeAheadAriaLabel="Organisation Name"
                  onToggle={onToggle}
                  onSelect={onSelect}
                  onClear={clearSelection}
                  selections={selectedParty}
                  isOpen={isPartyTypedownOpen}
                  aria-labelledby="party-typedown-1"
                  isInputValuePersisted={true}
                  placeholderText="Enter your organisation name"
                  isDisabled={false}
                  isCreatable={true}
                  onCreateOption={onCreateParty}
                >
                  {options.map((option, index) => (
                    <SelectOption key={option.id} value={option.value} index={index} />
                  ))}
                </Select>
              </DescriptionListDescription>
              <DescriptionListTerm>Additional Info</DescriptionListTerm>
              <DescriptionListDescription>
                <p>Please provide any additional information that will allow us to verify that you are allowed to
                   edit this data. An official email address of your organisation, or the contact details of your
                   data controller for example</p>
                <TextArea id="additional-request-details" autoResize="true" onChange={updateInfo}/>
              </DescriptionListDescription>
              <DescriptionListTerm>Next Steps</DescriptionListTerm>
              <DescriptionListDescription>
                <Text>
                  The site admins will review your request and may be in touch to verify the details. We will action your request as soon as possible.
                </Text>
              </DescriptionListDescription>
            </DescriptionListGroup>
          </DescriptionList>
        </Modal>
  )

  return (
    <>
      {renderAdoptText()}
      {adopt_modal}
    </>
  )
}

export default OwnershipInfo;
