import React, { useEffect, useState } from "react";
import { useKeycloak } from '@react-keycloak/ssr';
import { Button, Brand, Dropdown,
         DropdownItem,
         DropdownToggle,

  Masthead,
  MastheadMain,
  MastheadBrand,
  MastheadContent,
  MastheadToggle,
         Page,
         PageSection,
         PageHeader,
         PageHeaderTools,
         PageHeaderToolsItem,
         PageSidebar,
  Toolbar,
  ToolbarContent,
  ToolbarItem,
         Nav,
         NavItem,
         NavList } from '@patternfly/react-core';
import Link from 'next/link'
import UserMenu from './UserMenu'
import AdminMenu from './AdminMenu'
import axios from 'axios'
import { useRouter } from 'next/router'
import package_info from '../package.json'
import BarsIcon from '@patternfly/react-icons/dist/js/icons/bars-icon';

function MainLayout({Component, pageProps,cookies,children}) {

  const { keycloak, initialized } = useKeycloak()
  const user_data = initialized && keycloak.authenticated ? keycloak.tokenParsed : null;
  const is_admin = user_data?.realm_access?.roles?.includes('ADMIN')
  const router = useRouter()

  keycloak.onTokenExpired = () => {
    console.log('token expired', keycloak.token);
    keycloak.updateToken(30).success(() => {
      console.log('successfully get a new token', keycloak.token);
      axios.defaults.headers.common['Authorization'] = 'Bearer '+keycloak.token;
    }).error(() => {
      console.log("Problem refreshing token");
    });
  }

  if ( user_data != null ) {
    // console.log("Setting auth to %s",keycloak.token);
    axios.defaults.headers.common['Authorization'] = 'Bearer '+keycloak.token;
  }


  // Components removed - #42
  // NavItem key="auth" itemID={1} isActive={router.pathname?.startsWith('/authorities')}
  //   Link href="/authorities" a className="pf-c-nav__link" href="/authorities"Authorities/Link
  // /NavItem
  //

  const HeaderTools = (
    <Toolbar id="toolbar" isFullHeight isStatic>
      <ToolbarContent>
        <Nav variant="horizontal">
          <NavList>
            { is_admin && (
              // <NavItem key="admin" itemID={0} isActive={router.pathname?.startsWith('/admin')}>
              <NavItem key="admin" itemID={0}>
                <Link href="/admin">
                  <a className={`pf-c-nav__link ${router.pathname?.startsWith('/admin') ? ' pf-m-current' : ''}`} href="/admin">Admin</a>
                </Link>
              </NavItem>) }
            <ToolbarItem key="mult" itemID={2}>
              <Link href="/multiplexes"><a className="pf-c-nav__link" href="/multiplexes">Multiplexes</a></Link>
            </ToolbarItem>
            <ToolbarItem key="serv" itemID={3}>
              <Link href="/services"><a className="pf-c-nav__link" href="/services">Services</a></Link>
            </ToolbarItem>
            <ToolbarItem key="tran" itemID={4}>
              <Link href="/transmitters"><a className="pf-c-nav__link" href="/transmitters">Transmitters</a></Link>
            </ToolbarItem>
            <ToolbarItem key="contact" itemID={4}>
              <Link href="/contact"><a className="pf-c-nav__link" href="/contact">Contact</a></Link>
            </ToolbarItem>
          </NavList>
        </Nav>
        <UserMenu/>
      </ToolbarContent>
    </Toolbar>
  );


  const Header = (

      <Masthead id="basic">
        <MastheadToggle>
          <Button variant="plain" onClick={() => {}} aria-label="Global navigation">
            <BarsIcon />
          </Button>
        </MastheadToggle>
        <MastheadMain>
          <MastheadBrand>
            <Brand src="https://www.radioregister.org.uk" alt="RadioRegister" />
          </MastheadBrand>
        </MastheadMain>

        <MastheadContent>
          {HeaderTools}
        </MastheadContent>

      </Masthead>
  );

  const Sidebar = <PageSidebar nav="Navigation" isNavOpen={false} />;

  return (
    <Page mainContainerId="holoActiveComponent" header={Header} sidebar={Sidebar}>
      <PageSection isFilled={true} hasOverflowScroll={true}>
        {children}
      </PageSection>
      <PageSection>
        <div style={{"textAlign": "right"}}>
          RadioRegistry {package_info.version} ({process.env.NEXT_PUBLIC_ENV_LABEL}) &nbsp; | &nbsp;
          <Link href="/disclaimer"><a href="/disclaimer">Disclaimer: E&OE</a></Link>
        </div>
      </PageSection>
    </Page>
  )
}

export default MainLayout;
