const withTM = require('next-transpile-modules')([
  '@patternfly/react-core',
  '@patternfly/react-styles',
  '@patternfly/react-table'
])

const path = require('path')

// see: https://nextjs.org/docs/api-reference/next.config.js/runtime-configuration
module.exports = withTM({
  webpack5: true,
  sassOptions: {
    includePaths: [path.join(__dirname, 'react-styles')],
  },
  publicRuntimeConfig:{
    TESTVAR: process.env.TESTVAR,
    RR_API: process.env.RR_API
  }
})
